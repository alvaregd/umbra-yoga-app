package com.umbrayoga.umbrayoga.statics;


public class Keys {

    public final static String ACCOUNT_PREFERENCES = "ACCNT_PREFS";
    public final static String USR_OBJECTID   = "OBJID";
    public final static String USR_USERID     = "UID";
    public final static String USR_NAME       = "USRNAME";
    public final static String USR_EMAIL      = "EMAIL";
    public final static String USR_SCORE      = "SCORE";

}
