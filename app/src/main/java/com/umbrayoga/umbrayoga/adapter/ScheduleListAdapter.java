package com.umbrayoga.umbrayoga.adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.umbrayoga.umbrayoga.R;
import com.umbrayoga.umbrayoga.data.ScheduleData;
import com.umbrayoga.umbrayoga.utils.DateUtils;

import java.util.Calendar;
import java.util.Date;


public class ScheduleListAdapter extends ArrayAdapter<ScheduleData> {

    private Context context;

    //TODO organize that shit
    Typeface font;
    Calendar cal;

    public ScheduleListAdapter(Context context){
        super(context, R.layout.entry_schedule_list_);
        this.context= context;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if(convertView == null){
            LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.entry_schedule_list_,parent, false);
        }

        font = Typeface.createFromAsset(context.getAssets(),"EraserDust.ttf");

        ScheduleData data = getItem(position);
        TextView tvEventName = (TextView)convertView.findViewById(R.id.tv_event_name);
        tvEventName.setTypeface(font);
        tvEventName.setText(data.event_name);

        TextView tvEventDate = (TextView)convertView.findViewById(R.id.tv_event_date);
        tvEventDate.setTypeface(font);

       if(cal == null){
           cal = Calendar.getInstance();
       }

        cal.setTime(data.event_date);

        String dateString =

                DateUtils.Month(
                        Integer.valueOf(
                                (String)android.text.format.DateFormat.format("MM", data.event_date)
                        ) - 1
                )
                 + ", "
                 +  android.text.format.DateFormat.format("dd", data.event_date)

                 + " "
                 +  cal.get(Calendar.HOUR)
                 + ":"
                 + DateUtils.addZero(cal.get(Calendar.MINUTE))
                 + " "
                 +  DateUtils.AMPM(cal.get(Calendar.AM_PM));

        tvEventDate.setText(dateString);


        return convertView;
    }


    @Override
    public ScheduleData getItem(int position) {
        return super.getItem(position);
    }
}
