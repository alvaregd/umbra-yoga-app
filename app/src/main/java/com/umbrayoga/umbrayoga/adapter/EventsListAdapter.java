package com.umbrayoga.umbrayoga.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.TextView;

import com.umbrayoga.umbrayoga.R;
import com.umbrayoga.umbrayoga.data.ScheduleData;
import com.umbrayoga.umbrayoga.utils.DateUtils;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

public class EventsListAdapter extends BaseExpandableListAdapter {

    private final static String Tag = "EventsListAdapter";
    private final static boolean d_getChildView = true;

    private Context context;
    private int selectedItem = 0;

    List<String> mDays;  //Group names
    HashMap<String, List<ScheduleData>> mClasses; //child names


    public EventsListAdapter(Context context) {
//        super(context, R.layout.entry_picker_list);
        this.context = context;
        mDays = new ArrayList<>();
        mClasses = new HashMap<>();
    }

    public void addAll(ScheduleData... data){

        /** for each event, get the date. and check the date against the
         * list of existing dates. If it does not exist, make a new list and
         * add the even to that list
         */

        /** clear previous entries*/
        mDays.clear();
        mClasses.clear();

        for(int i = 0; i < data.length; i++){

            Log.e(Tag, "Adding:" + data[i].event_name);

            Date date =   data[i].event_date;

            String dateString =  DateUtils.Month(
                    Integer.valueOf(
                            (String) android.text.format.DateFormat.format("MM", date)
                         ) - 1
                      )
                    + " "
                    +  android.text.format.DateFormat.format("dd", date);

            /** Does this group already exist ?*/
            if(!mDays.contains(dateString)){
                //NO

                /** create a new group */
                mDays.add(dateString);

                /** this day is not found, create a new child collection*/
                List childgroup = new ArrayList<>();
                /** place our schedule data into the child collection */
                childgroup.add(data[i]);
                /** save our child colleciton */
                mClasses.put(dateString, childgroup);

            }else{
                //YES
                List<ScheduleData> collection = mClasses.get(dateString);
                collection.add(data[i]);
            }
        }
    }

  /*  @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.entry_picker_list, parent, false);
        }

        *//** set the color's name *//*
//        ((TextView) convertView.findViewById(R.id.tv_entry_name)).setText(getItem(position).event_name);

        *//** Set it so that only one entry is selectable at a time. We indicate the selected
         * item via an indicator, which is set to invisible in all except the selected entry *//*
        if (position == selectedItem)
            convertView.findViewById(R.id.iv_indicator).setVisibility(View.VISIBLE);
        else {
            convertView.findViewById(R.id.iv_indicator).setVisibility(View.INVISIBLE);
        }
        return convertView;
    }*/

    public void setSelectedEntry(int selected) {
        this.selectedItem = selected;
    }

    @Override
    public int getGroupCount() {
        return mDays.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return mClasses.get(mDays.get(groupPosition)).size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return mDays.get(groupPosition);
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return mClasses.get(mDays.get(groupPosition)).get(childPosition);
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        if(convertView == null){
            LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.entry_picker_list,parent, false);
        }

        ((TextView)convertView.findViewById(R.id.tv_entry_name)).setText((String)getGroup(groupPosition));
        /** Set it so that only one entry is selectable at a time. We indicate the selected
         * item via an indicator, which is set to invisible in all except the selected entry */
        if(groupPosition == selectedItem)
            convertView.findViewById(R.id.iv_indicator).setVisibility(View.VISIBLE);
        else {
            convertView.findViewById(R.id.iv_indicator).setVisibility(View.INVISIBLE);
        }
        return convertView;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {

        if(convertView == null){
            LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.entry_picker_child,parent, false);
        }

        ScheduleData data= ((ScheduleData)getChild(groupPosition, childPosition));

        Calendar cal = Calendar.getInstance();
        cal.setTime(data.event_date);
        String time = cal.get(Calendar.HOUR)
                + ":"
                + DateUtils.addZero(cal.get(Calendar.MINUTE))
                + " "
                +  DateUtils.AMPM(cal.get(Calendar.AM_PM));

        if(d_getChildView)Log.e(Tag,"requested:" + groupPosition + ", " + childPosition + " " + data.event_name);

        TextView tvEventName = (TextView)convertView.findViewById(R.id.tv_event_name);

        tvEventName.setText(
                "(" + data.event_current_count + "/" + data.event_max_count + ")" + " " +
                        data.event_name);

        TextView tvEventDate = (TextView)convertView.findViewById(R.id.tv_event_date);
        tvEventDate.setText(time);

        return convertView;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }


    public void UpdateCounter(int group, int child){
       ScheduleData data = (ScheduleData) getChild(group,child);
        data.event_current_count++;
        notifyDataSetChanged();
    }



}
