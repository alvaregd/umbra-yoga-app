package com.umbrayoga.umbrayoga.adapter;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.umbrayoga.umbrayoga.R;
import com.umbrayoga.umbrayoga.fragment.PickerFragment;

import java.util.Locale;

/**
 * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
 * one of the sections/tabs/pages.
 */
public class BookFlowAdapter extends FragmentPagerAdapter {

    public final static int STUDIO   = 0;
    public final static int ACTIVITY = 1;
    public final static int TIMESLOT = 2;
    private Context context;

    public BookFlowAdapter(FragmentManager fm, Context context) {
        super(fm);
        this.context = context;
    }

    @Override
    public Fragment getItem(int position) {

        switch(position){
            case STUDIO:


                break;

            case ACTIVITY:

                break;
            case TIMESLOT:


                break;
        }
        return PickerFragment.newInstance(position);
    }

    @Override
    public int getCount() {
        // Show 3 total pages.
        return 3;
    }


}