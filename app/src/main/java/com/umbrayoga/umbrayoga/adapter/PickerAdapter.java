package com.umbrayoga.umbrayoga.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.umbrayoga.umbrayoga.R;
import com.umbrayoga.umbrayoga.data.KeyValuePair;

public class PickerAdapter extends ArrayAdapter<KeyValuePair> {

    private Context context;
    private int selectedItem = 0;

    public PickerAdapter(Context context){
        super(context, R.layout.entry_picker_list);
        this.context = context;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if(convertView == null){
            LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.entry_picker_list,parent, false);
        }

        /** set the color's name */
        ((TextView)convertView.findViewById(R.id.tv_entry_name)).setText(getItem(position).key);

        /** Set it so that only one entry is selectable at a time. We indicate the selected
         * item via an indicator, which is set to invisible in all except the selected entry */
        if(position == selectedItem)
            convertView.findViewById(R.id.iv_indicator).setVisibility(View.VISIBLE);
        else {
            convertView.findViewById(R.id.iv_indicator).setVisibility(View.INVISIBLE);
        }


        return convertView;
    }

    public void setSelectedEntry(int selected){
        this.selectedItem = selected;

    }
}
