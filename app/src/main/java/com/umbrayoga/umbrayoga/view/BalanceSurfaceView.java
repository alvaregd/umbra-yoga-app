package com.umbrayoga.umbrayoga.view;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PixelFormat;
import android.graphics.PorterDuff;
import android.util.AttributeSet;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import com.umbrayoga.umbrayoga.callbacks.GameListener;

import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by alvaregd on 04/06/15.
 */
public class BalanceSurfaceView extends SurfaceView implements Runnable {

    private final static String Tag = "SurfaceView";
    private final static boolean d_run = true;

    private final static int ASSET_SPRITE = 0;

    private final static int STATE_RUNNING = 0;
    private final static int STATE_IDLE = 1;
    private final static int STATE_DIED = 2;

    private int GameState = 0;

    Paint textPaint;
    private String scoreText = "0";
    private String highScoreText ="";
    private int score = 0;
    private int highScore = 0;
    private int TEXT_45 = 45;
    private int TEXT_60 = 60;
    private GameListener mListener;

    SurfaceHolder ourHolder;
    Thread ourThread;
    Canvas canvas;
    boolean isRunning = false;
    boolean isResized = false;
    Person person;

    float sensorZ;

    Timer timer;
    TimerTask timerTask;
    static boolean isTimerRunning;

    public BalanceSurfaceView(Context paramContext) {

        this(paramContext, null);
        init();
    }

    public BalanceSurfaceView(Context paramContext, AttributeSet paramAttributeSet) {
        this(paramContext, paramAttributeSet, 0);
        init();
    }

    public BalanceSurfaceView(Context paramContext, AttributeSet paramAttributeSet, int paramInt) {
        super(paramContext, paramAttributeSet, paramInt);
        init();
    }

    private void init(){
        textPaint = new Paint();
        GameState = STATE_RUNNING;
        timer = new Timer();
        timerTask = newTimerTask();
    }

    private TimerTask newTimerTask(){
        return  new TimerTask() {
            @Override
            public void run() {
                score++;

                if(score > highScore){
                    highScore = score;
                    highScoreText = ""+score/10 + "." + score % 10;
                }
                scoreText = ""+score/10 + "." + score % 10;
            }
        };
    }


    public void updateSensorValues(float sensorZ, float filtered){
        this.sensorZ = sensorZ;
    }

    public void injectListener(GameListener listener){
        mListener = listener;

    }

    public void injectAssets(Bitmap[] images){
        if(person == null) person = new Person();
        person.injectAssets(images);
    }

    public void ResizeAssets(){

        int canvaswidth = canvas.getWidth();
        int canvasHeight = canvas.getHeight();

        if(person == null)person = new Person();
        person.Resize(canvaswidth, canvasHeight);
    }

    public void pause(){

        isResized = false;
        isRunning = false;
        try{
            ourThread.join();
        }catch (InterruptedException e){

        }
        ourThread = null;
        StopTimer();
        mListener.SaveHighScore(highScore);
    }

    public void resume(){

        GameState = STATE_IDLE;
        ourHolder = this.getHolder();
        isRunning = true;
        ourThread = new Thread(this);
        ourThread.start();
//        StartTimer();
        highScore = mListener.getHighScore();
        highScoreText = ""+ highScore/10 +"." + highScore%10;
    }

    public void StartTimer(){
        timer = new Timer();
        timer.scheduleAtFixedRate(newTimerTask(),100,100);
        isTimerRunning = true;
        score = 0;
        scoreText = "0";
    }
    public void StopTimer(){
        timer.cancel();
        isTimerRunning = false;
    }

    @Override
    public void run() {

        while(isRunning){
            Update();
            if(ourHolder == null){
                continue;
            }
            ourHolder.setFormat(PixelFormat.TRANSPARENT);
            if(!ourHolder.getSurface().isValid()){
                continue;
            }

            canvas = ourHolder.lockCanvas();
            if(!isResized){
                isResized = true;
                ResizeAssets();
            }

            canvas.drawColor(Color.TRANSPARENT, PorterDuff.Mode.CLEAR);
            DrawSprites(canvas);
            updateTimer(canvas);
            ourHolder.unlockCanvasAndPost(canvas);
        }
    }

    public void Update(){
        if(person.Update(sensorZ) && GameState == STATE_RUNNING){

            Log.e(Tag,"STATE DIED");
            GameState = STATE_DIED;
            StopTimer();
        }
    }

    public void DrawSprites(Canvas canvas){
        person.Draw(canvas);
    }

    public void NextState(){
        switch(GameState){

            case STATE_IDLE:
                GameState = STATE_RUNNING;
            case STATE_DIED:
            case STATE_RUNNING:
                person.Reset();
                GameState = STATE_RUNNING;

                if(isTimerRunning){
                    StopTimer();
                }
                StartTimer();
                break;
        }
    }

    public void updateTimer(Canvas canvas){

        switch(GameState){
            case STATE_IDLE:
                textPaint.setColor(Color.GREEN);
                textPaint.setTextSize(TEXT_60);
                canvas.drawText("Best:  " + highScoreText, canvas.getWidth() / 2 - (textPaint.measureText("Best:  " + highScoreText) / 2), canvas.getHeight() / 2 + 50, textPaint);
                break;
            case STATE_RUNNING:

                /** paint the current score */
                textPaint.setColor(Color.GREEN);
                textPaint.setTextSize(TEXT_45);
                canvas.drawText(scoreText, 50, 50, textPaint);
                canvas.drawText("Best: " +highScoreText, canvas.getWidth() - textPaint.measureText("Best: " + highScoreText) , 50, textPaint);
                break;

            case STATE_DIED:
                /** paint the current score */
                textPaint.setColor(Color.GREEN);
                textPaint.setTextSize(TEXT_60);
                canvas.drawText("Score: " + scoreText, canvas.getWidth() / 2 - (textPaint.measureText("Score:" +scoreText) / 2), canvas.getHeight() / 2, textPaint);
                canvas.drawText("Best:  " +highScoreText, canvas.getWidth() / 2 - (textPaint.measureText("Best:  " +highScoreText) / 2), canvas.getHeight() / 2 + 50 , textPaint);
                break;
        }
    }



}
