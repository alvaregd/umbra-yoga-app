package com.umbrayoga.umbrayoga.view;

import android.graphics.Bitmap;
import android.graphics.Camera;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.util.Log;

import java.util.Random;

/**
 * Created by alvaregd on 04/06/15.
 */


public class Person {

    private final static String Tag = "Person";

    /** Dimension specifications */
    private final static float WIDTH_PERCENT = 1f;
    private final static float HEIGHT_PERCENT = 1f;

    /** rotation constants */
    public final static float MAX_ANGLE = 1.3707f;


    Bitmap workingBitmap;
    Bitmap[] bmpCollection;
    static int PositionX;
    static int PositionY;

    static Matrix rotMatrix;
    static Camera rotCamera;

    static int canvasWidth;
    static int canvasHeight;

    static Random rand;

    static float angularPosition_i = 0f; //initial angular position
    static float angularVelocity_i = 0f; //initial angular velocity
    static float angularAcceleration_i = 0f; //initial angular acceleration
    static float angularPosition_f = 0f; //final angular position
    static float angularVelocity_f =0f ; //final angular position
    static float angularAcceleration_f =0f; //final angular position
    static final float GRAVITY = 0.16f; //gravity constant
    static float bodyLength  = 0.2f;    //length of person
    static final float rate_of_motion = 0.1f;

    static float Sensor;

    private static boolean isDead = false;

    private boolean firstTime = true;

    public Person(){
        rotMatrix = new Matrix();
        rotCamera = new Camera();
        rand = new Random();

        firstTime = true;

        isDead = false;
        angularPosition_i = 0f; //initial angular position
        angularVelocity_i = 0f; //initial angular velocity
        angularAcceleration_i = 0f; //initial angular acceleration
        angularPosition_f = 0f; //final angular position
        angularVelocity_f =0f ; //final angular position
        angularAcceleration_f =0f; //final angular position
        bodyLength  = 0.2f;    //length of person

    }

    public void injectAssets(Bitmap[] collection){
        this.bmpCollection = collection;
        setWorkingBitmap();

    }

    private void setWorkingBitmap(){
        this.workingBitmap = bmpCollection[rand.nextInt(bmpCollection.length )];
    }

    public void Reset(){

         setWorkingBitmap();
         isDead = false;
         angularPosition_i = 0f; //initial angular position
         angularVelocity_i = 0f; //initial angular velocity
         angularAcceleration_i = 0f; //initial angular acceleration
         angularPosition_f = 0f; //final angular position
         angularVelocity_f =0f ; //final angular position
         angularAcceleration_f =0f; //final angular position
         bodyLength  = 0.2f;    //length of person
    }

    public void Resize(int canvaswidth, int canvasheight){

        for(int i = 0; i < bmpCollection.length; i++){
            bmpCollection[i] = Bitmap.createScaledBitmap(bmpCollection[i], Math.round(canvaswidth * WIDTH_PERCENT), Math.round(canvasheight * HEIGHT_PERCENT), false);
        }
        canvasWidth = canvaswidth;
        canvasHeight = canvasheight;
        Reset();
    }

    public boolean Update(float sensorZ){

        /** The sensor value will be act as an external force on our rigid body */
        if(sensorZ < 0.15 && sensorZ > -0.15 ) Sensor = 0;
        else Sensor = sensorZ;

        isDead = false;

        /** add the acceleration from the sensor */
      //  Log.e(Tag, "Sensor:" +  Sensor);

        if(angularPosition_i ==0){
            angularPosition_i = 0.0009f;
        }

        /** perform calculations */
        angularAcceleration_f = Sensor*0.02f +  (3/2) * GRAVITY * bodyLength * (float)Math.sin(angularPosition_i);
        angularVelocity_f = angularVelocity_i + (angularAcceleration_f*rate_of_motion);
        angularPosition_f = angularPosition_i + (angularVelocity_f*rate_of_motion);

        /** make the previous final values our initial values for the next iteration*/
        angularAcceleration_i = angularAcceleration_f;
        angularVelocity_i = angularVelocity_f;
        angularPosition_i = angularPosition_f;

        /** put a limit on the amount of rotation possible */

//        Log.e(Tag,"POS: " + angularPosition_f);

        if(angularPosition_f >= MAX_ANGLE){
//            Log.e(Tag,"OVER");
            angularPosition_f = MAX_ANGLE;
            angularVelocity_f = 0;
            angularAcceleration_f = 0;
            death();
        }else if (angularPosition_f <= -MAX_ANGLE){

//            Log.e(Tag,"UNDER ");
            angularPosition_f = -MAX_ANGLE;
            angularVelocity_f = 0;
            angularAcceleration_f =0;
            death();
        }else{
            isDead = false;
        }

        /** Rotate the View */
        rotCamera.save();
        rotCamera.rotateZ(angularPosition_f*180/3.14159f);
        rotCamera.getMatrix(rotMatrix);
        rotCamera.restore();

        rotMatrix.preTranslate(-workingBitmap.getWidth() / 2, -workingBitmap.getHeight());
        /** translate the view to the bottom center */
        rotMatrix.postTranslate(canvasWidth/2 ,  canvasHeight );

        return isDead;
    }

    public void death(){

        isDead = true;
//        Log.e(Tag,"IS DEAD");
    }


    public void Draw(Canvas canvas){
        canvas.drawBitmap(workingBitmap, rotMatrix,null);
    }
}
