package com.umbrayoga.umbrayoga.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;

import com.umbrayoga.umbrayoga.R;

/**
 * Created by alvaregd on 04/06/15.
 */
public class TabDotIndicator3 extends FrameLayout {

    private Context context;

    View tab1Guide, tab2Guide, tab3Guide;

    public TabDotIndicator3(Context context) {
        super(context);
        initialize(context);
        this.context = context;
    }

    public TabDotIndicator3(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        this.context = context;
        initialize(context);
    }

    public TabDotIndicator3(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        initialize(context);
    }

    public void initialize(Context context) {
        LayoutInflater lif = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        lif.inflate(R.layout.tab_indicator_3, this);

        //initialize the buttons
        tab1Guide = (View) this.findViewById(R.id.tab_1_guide);
        tab2Guide = (View) this.findViewById(R.id.tab_2_guide);
        tab3Guide = (View) this.findViewById(R.id.tab_3_guide);

        tab1Guide.setBackgroundResource(R.drawable.tiny_circle_grey);
        tab2Guide.setBackgroundResource(R.drawable.tiny_hollow_circle_grey);
        tab3Guide.setBackgroundResource(R.drawable.tiny_hollow_circle_grey);

    }

    public void setguide(int select) {

        tab1Guide.setBackgroundResource((select == 0) ?  R.drawable.tiny_circle_grey :  R.drawable.tiny_hollow_circle_grey);
        tab2Guide.setBackgroundResource((select == 1) ?  R.drawable.tiny_circle_grey :  R.drawable.tiny_hollow_circle_grey);
        tab3Guide.setBackgroundResource((select == 2) ?  R.drawable.tiny_circle_grey :  R.drawable.tiny_hollow_circle_grey);

    }

}