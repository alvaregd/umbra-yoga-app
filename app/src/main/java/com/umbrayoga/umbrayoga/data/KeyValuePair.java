package com.umbrayoga.umbrayoga.data;

/**
 * Created by alvaregd on 04/06/15.
 */
public class KeyValuePair {


    public KeyValuePair(String key, int value ){

        this.key = key;
        this.value = value;
    }

    public String key;
    public int value;
}
