package com.umbrayoga.umbrayoga.data;

import java.util.Date;

public class ScheduleData {

    public ScheduleData(){

    }

    public ScheduleData(String name, Date date, String evenLocation){
        this.event_location= evenLocation;
        this.event_date = date;
        this.event_name = name;
    }

    public String event_location;
    public String event_name;
    public Date event_date;
    public String event_Id;
    public String event_instructor;
    public String event_description;
    public int event_max_count;
    public int event_current_count;

    public String attedance_objectId;

}
