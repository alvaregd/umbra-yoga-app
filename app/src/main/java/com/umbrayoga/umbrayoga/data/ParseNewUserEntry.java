package com.umbrayoga.umbrayoga.data;

/**
 * Created by alvaregd on 06/06/15.
 */
public class ParseNewUserEntry {

    public ParseNewUserEntry(String email , String password ,String name, int score){
        this.name= name;
        this.password = password;
        this.email = email;
        this.score = score;
    }

    public String name;
    public String password;
    public String email;
    public int score;

}
