package com.umbrayoga.umbrayoga.data;


public class BookingData {

    public final static int YOGA   = 0;
    public final static int JAB    = 0;

    public int selectedStudioIndex    = -1;
    public int selectedActivityIndex  = -1;
    public int selectedTimeIndex      = -1;

    public  String studio;
    public  String activity;
    public  int time;
}
