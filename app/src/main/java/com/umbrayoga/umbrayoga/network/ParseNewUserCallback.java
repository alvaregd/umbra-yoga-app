package com.umbrayoga.umbrayoga.network;

/**
 * Created by alvaregd on 06/06/15.
 */
public interface ParseNewUserCallback {

     void NewUserSuccess(int id, String object, String name, String email);
     void NewUserError(String error);

}
