package com.umbrayoga.umbrayoga.network;

import com.umbrayoga.umbrayoga.data.ScheduleData;


public interface ParseFetchFutureEventsCallback {

    void onSuccessEvents(ScheduleData[] data);
    void onFailEvents(String error);
}
