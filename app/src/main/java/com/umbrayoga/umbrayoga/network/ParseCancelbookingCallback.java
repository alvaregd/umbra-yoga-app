package com.umbrayoga.umbrayoga.network;

/**
 * Created by alvaregd on 09/06/15.
 */
public interface ParseCancelbookingCallback {

    void onCancelSuccess();
    void onCancelFail(String message);
}
