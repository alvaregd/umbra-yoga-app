package com.umbrayoga.umbrayoga.network;

import com.umbrayoga.umbrayoga.data.ScheduleData;

/**
 * Created by alvaregd on 07/06/15.
 */
public interface ParseFetchEarliestEventCallback {

    void OnFetchEarliestSuccess(ScheduleData data);
    void OnFetchEarliestFail(String error);

}
