package com.umbrayoga.umbrayoga.network;

import com.umbrayoga.umbrayoga.data.ScheduleData;


public interface ParseFetchEventsCallback {

     void OnFetchEventReceived(ScheduleData[] data);
     void OnError(String description);

}
