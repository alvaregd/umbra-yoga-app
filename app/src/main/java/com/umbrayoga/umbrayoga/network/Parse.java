package com.umbrayoga.umbrayoga.network;


import android.util.Log;

import com.parse.FindCallback;
import com.parse.LogInCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.SignUpCallback;
import com.umbrayoga.umbrayoga.data.ActivityData;
import com.umbrayoga.umbrayoga.data.ParseNewUserEntry;
import com.umbrayoga.umbrayoga.data.ScheduleData;
import com.umbrayoga.umbrayoga.data.StudioData;

import java.util.Date;
import java.util.List;

public class Parse {

    private final static String Tag = "Parse";
    private final static boolean d_fetchMyEvents = true;
    private final static boolean d_fetchMyEarliestEvents = true;
    private final static boolean d_newAccount =true;
    private final static boolean d_getActiviyType = true;
    private final static boolean d_getAlLEvents = true;
    private final static boolean d_getStudios = true;
    private final static boolean d_bookClass  = true;
    private final static String OBJECT_ID = "objectId";

    /** keys reelated to the USER_COUNT class */
    private final static String CLASS_USR_COUNT = "User_count";
    private final static String USRCOUNT_COUNT = "count";

    /** Keys related to the user classs*/
    private final static String CLASS_USER    = "_User";
    private final static String USER_USERID   = "userId";
    private final static String USER_EVENTS   = "events";
    private final static String USER_NAME     = "username";
    private final static String USER_REALNAME = "name";
    private final static String USER_SCORE    = "topScore";

    /** keys related to the Events class */
    private final static String CLASS_EVENTS = "Events";
    private final static String EVENT_NAME   = "name";
    private final static String EVENT_TIME   = "time";
    private final static String EVENT_LOCA   = "location";
    private final static String EVENT_INSTR  = "Instructor";
    private final static String EVENT_DESC  = "description";
    private final static String EVENT_MAX  = "max_attendees";
    private final static String EVENT_CNT  = "attendeeCount";


    /** keys related to the attendance  class */
    private final static String CLASS_ATTEND = "Attendance";
    private final static String ATTEN_USERID = "userId";
    private final static String ATTEN_EVENTID = "EventId";
    private final static String ATTEN_TIME = "eventTime";
    private final static String ATTEN_NAME = "eventName";
    private final static String ATTEN_LOCA = "eventLocation";

    /** keys related to the EVENT_LOCATION class */
    private final static String CLASS_LOCATION = "Event_Location";
    private final static String LOCATION_ADDRESS = "address";
    private final static String LOCATION_STUDIONAME = "studioName";

    /** keys related to the EVENT_TYPE class */
    private final static String CLASS_EVENT_TYPE = "Event_Types";
    private final static String TYPE_NAME = "name";


    /**Not to be confused with fetchMyEarliestEvent, this gets Latest event EVER*/
    public static void fetchAnyEarliestEvents(final ParseFetchEarliestEventCallback callback){

        if(d_fetchMyEarliestEvents)Log.e(Tag,"fetchEarliest() ");
        Date dateTimeNow = new Date();

        ParseQuery<ParseObject> attendQuery = ParseQuery.getQuery(CLASS_EVENTS);
        attendQuery.whereGreaterThan(EVENT_TIME,dateTimeNow);
        attendQuery.findInBackground(new FindCallback<ParseObject>() {

            @Override
            public void done(List<ParseObject> list, ParseException e) {
                if(e == null){
                    if(list.size() <= 0){
                        callback.OnFetchEarliestFail("No Events Entries");
                    }else{
                        if(d_fetchMyEarliestEvents)  Log.e(Tag,"Size:" + list.size());

                        /** prepare our variables */
                        ParseObject  attendances;
                        ScheduleData earliestData = new ScheduleData();

                        /** prepare for the loop */
                        attendances= list.get(0);
                        earliestData.event_date = attendances.getDate(EVENT_TIME);
                        earliestData.event_name = attendances.getString(EVENT_NAME);
                        earliestData.event_location = attendances.getString(EVENT_LOCA);

                        for(int i=1; i<list.size();i++){
                            attendances = list.get(i);

                            if(attendances.getDate(EVENT_TIME).before(earliestData.event_date)){
                                earliestData.event_date = attendances.getDate(EVENT_TIME);
                                earliestData.event_name = attendances.getString(EVENT_NAME);
                                earliestData.event_location = attendances.getString(EVENT_LOCA);
                            }
                        }

                        callback.OnFetchEarliestSuccess(earliestData);
                    }
                }else{
                    callback.OnFetchEarliestFail("Error @ 1st Stage");
                }
            }
        });
    }

    /** Get a user's easliest possible event*/
    public static void fetchMyEarliestEvents(int userId, final ParseFetchEarliestEventCallback callback){

        if(d_fetchMyEarliestEvents)Log.e(Tag,"fetchEarliest() Enter Id:" + userId);

        Date dateTime = new Date();
        ParseQuery<ParseObject> attendQuery = ParseQuery.getQuery(CLASS_ATTEND);
        attendQuery.whereEqualTo(ATTEN_USERID, userId);
        attendQuery.whereGreaterThan(ATTEN_TIME,dateTime);
        attendQuery.findInBackground(new FindCallback<ParseObject>() {

            @Override
            public void done(List<ParseObject> list, ParseException e) {
                if(e == null){
                    if(list.size() <= 0){
                        callback.OnFetchEarliestFail("No Events Attenance Entries");
                    }else{
                        if(d_fetchMyEarliestEvents)  Log.e(Tag,"Size:" + list.size());

                        /** prepare our variables */
                        ParseObject  attendances;
                        ScheduleData earliestData = new ScheduleData();

                        /** prepare for the loop */
                        attendances= list.get(0);
                        earliestData.event_date = attendances.getDate(ATTEN_TIME);
                        earliestData.event_name = attendances.getString(ATTEN_NAME);
                        earliestData.event_location = attendances.getString(ATTEN_LOCA);

                        for(int i=1; i<list.size();i++){
                            attendances = list.get(i);

                            if(attendances.getDate(ATTEN_TIME).before(earliestData.event_date)){
                                earliestData.event_date = attendances.getDate(ATTEN_TIME);
                                earliestData.event_name = attendances.getString(ATTEN_NAME);
                                earliestData.event_location = attendances.getString(ATTEN_LOCA);
                            }
                        }
                        callback.OnFetchEarliestSuccess(earliestData);
                    }
                }else{
                    callback.OnFetchEarliestFail("Error @ 1st Stage");
                }
            }
        });
    }

    /** Get a user's information */
    public static void fetchMyEvents(int userId, final ParseFetchEventsCallback callback){

        Date dateTime = new Date();
        ParseQuery<ParseObject> attendQuery = ParseQuery.getQuery(CLASS_ATTEND);
        attendQuery.whereEqualTo(ATTEN_USERID, userId);
        attendQuery.whereGreaterThan(ATTEN_TIME, dateTime);
        attendQuery.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> list, ParseException e) {
                if (e == null) {
                    if (list.size() <= 0) {
                        callback.OnError("No Events attendance Entries");
                    } else {
                        if (d_fetchMyEarliestEvents) Log.e(Tag, "Size:" + list.size());

                        /** prepare our variables */
                        ParseObject attendances;
                        ScheduleData[] data = new ScheduleData[list.size()];

                        /** prepare for the loop */
                        for (int i = 0; i < list.size(); i++) {

                            attendances = list.get(i);
                            ScheduleData scheduleData = new ScheduleData();
                            scheduleData.event_date = attendances.getDate(ATTEN_TIME);
                            scheduleData.event_name = attendances.getString(ATTEN_NAME);
                            scheduleData.event_location = attendances.getString(ATTEN_LOCA);
                            scheduleData.event_Id = attendances.getString(ATTEN_EVENTID);
                            scheduleData.attedance_objectId = attendances.getObjectId();
                            data[i] = scheduleData;
                        }
                        callback.OnFetchEventReceived(data);
                    }
                } else {
                    callback.OnError("Error @ 1st Stage");
                }
            }
        });
     }

    /** make new user in parse */
    public static void createNewUser(final ParseNewUserEntry data, final ParseNewUserCallback callback) {

        /** get the counter on parse */
        ParseQuery<ParseObject> idcounter = ParseQuery.getQuery(CLASS_USR_COUNT);
        idcounter.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> parseObjects, ParseException e) {
                if (e == null) {

                    if (d_newAccount) Log.e(Tag, "making a new account..");

                    /** increment the counter */
                    ParseObject parseObject = parseObjects.get(0);
                    final int id = parseObject.getInt(USRCOUNT_COUNT);
                    parseObject.increment(USRCOUNT_COUNT);
                    parseObject.saveInBackground();

                    /** create a new user */
                    final ParseUser newEntry = new ParseUser();
                    newEntry.setUsername(data.email);
                    newEntry.setEmail(data.email);
                    newEntry.setPassword(data.password);
                    newEntry.put(USER_USERID, id);
                    newEntry.put(USER_REALNAME, data.name);
                    newEntry.put(USER_SCORE, data.score);

                    if (d_newAccount) Log.e(Tag, "email: " + data.email);
                    if (d_newAccount) Log.e(Tag, "name: " + data.name);
                    if (d_newAccount) Log.e(Tag, "score: " + data.score);
                    if (d_newAccount) Log.e(Tag, "id: " + id);

                    newEntry.signUpInBackground(new SignUpCallback() {
                        @Override
                        public void done(com.parse.ParseException e) {
                            if (e == null) {
                                if (d_newAccount) Log.e(Tag, "Signed up in background");
                                /** let the app know what happened */
                                callback.NewUserSuccess(
                                        id,
                                        newEntry.getObjectId(),
                                        data.name,
                                        data.email
                                );
                            } else {
                                callback.NewUserError(e.getMessage());
                            }
                        }
                    });
                }
            }
        });
    }

    /** Handle Relogin request */
    public static void ReLogin(String[] credentials, final ParseLoginCallBack listener){
        /** find the user */
        ParseUser.logInInBackground(credentials[0], credentials[1], new LogInCallback() {
            public void done(ParseUser user, ParseException e) {
                //TODO FIX PROFILE ID QUERY
                if (user != null) {
                    /** User exists and credentials worked, now fetch the user's profile data */
                    listener.onLoginSuccess(
                            user.getInt(USER_USERID),
                            user.getObjectId(),
                            user.getString(USER_REALNAME),
                            user.getString(USER_NAME),
                            user.getInt(USER_SCORE)
                    );

                } else {
                    /** let the app know what happened (failed) */
                    listener.onLoginFail(e.getMessage());
                }
            }
        });
    }

    /** Load a single event detail */
    public static void LoadEventDetail(String objectId, final ParseEventDetailCallback callback){

        Log.e(Tag, "Searching with:" + objectId);

        ParseQuery<ParseObject> attendQuery = ParseQuery.getQuery(CLASS_EVENTS);
        attendQuery.whereEqualTo(OBJECT_ID, objectId);
        attendQuery.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> list, ParseException e) {
                if (e == null) {
                    if (list.size() != 1) {

                        callback.onFail("found many or no matches");

                    } else {
                        if (d_fetchMyEarliestEvents) Log.e(Tag, "Size:" + list.size());

                        /** prepare our variables */
                        ParseObject attendances;
                        ScheduleData detail = new ScheduleData();

                        /** prepare for the loop */
                        attendances = list.get(0);
                        detail.event_date = attendances.getDate(EVENT_TIME);
                        detail.event_name = attendances.getString(EVENT_NAME);
                        detail.event_location = attendances.getString(EVENT_LOCA);
                        detail.event_instructor = attendances.getString(EVENT_INSTR);
                        detail.event_description = attendances.getString(EVENT_DESC);

                        callback.onSuccess(detail);
                    }
                } else {
                    callback.onFail("Error @ 1st Stage");
                }
            }
        });

    }

    public static void FetchStudio(final ParseFetchStudioCallback callback){

        ParseQuery<ParseObject> studioData = ParseQuery.getQuery(CLASS_LOCATION);
        studioData.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> list, ParseException e) {
                if (e == null) {
                    if (list.size() < 1) {
                        callback.onFailStudio("found no matches");

                    } else {

                        if (d_fetchMyEarliestEvents) Log.e(Tag, "Size:" + list.size());

                        ParseObject  studios;
                        StudioData[] data  = new StudioData[list.size()];

                        /** prepare for the loop */
                        for(int i=0; i<list.size();i++) {

                            studios = list.get(i);
                            StudioData studioData    = new StudioData();
                            studioData.objectId      = studios.getObjectId();
                            studioData.studioName    = studios.getString(LOCATION_STUDIONAME);
                            studioData.studioaddress = studios.getString(LOCATION_ADDRESS);
                            data[i] = studioData;
                        }
                        callback.onSuccessStudio(data);
                    }
                } else {
                    callback.onFailStudio(e.getMessage());
                }
            }
        });
    }

    public static void FetchActivity(final ParseFetchActivityCallback callback){

        if(d_getActiviyType)Log.e(Tag, "FetchActivity() enter size");

        ParseQuery<ParseObject> studioData = ParseQuery.getQuery(CLASS_EVENT_TYPE);
        studioData.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> list, ParseException e) {
                if (e == null) {
                    if (list.size() < 1) {
                        callback.onFailActivity("found no matches");
                    } else {

                        if (d_getActiviyType) Log.e(Tag, "Size:" + list.size());

                        ParseObject  activities;
                        ActivityData[] data  = new ActivityData[list.size()];

                        /** prepare for the loop */
                        for(int i=0; i<list.size();i++) {

                            activities = list.get(i);
                            ActivityData activityData    = new ActivityData();
                            activityData.objectId      = activities.getObjectId();
                            activityData.activityName    = activities.getString(TYPE_NAME);
                            data[i] = activityData;
                        }
                        callback.onSuccessActivity(data);
                    }
                } else {
                    callback.onFailActivity(e.getMessage());
                }
            }
        });
    }

    /** gets all of the events given the search criteria */
    public static void fetchAllEvents(final ParseFetchFutureEventsCallback callback){

        if(d_getAlLEvents)Log.e(Tag,"fetchAllEvents() ");
        Date dateTimeNow = new Date();

        ParseQuery<ParseObject> attendQuery = ParseQuery.getQuery(CLASS_EVENTS);
        attendQuery.whereGreaterThan(EVENT_TIME, dateTimeNow);
        attendQuery.findInBackground(new FindCallback<ParseObject>() {

            @Override
            public void done(List<ParseObject> list, ParseException e) {
                if(e == null){
                    if(list.size() <= 0){
                        callback.onFailEvents("No Events Entries");
                    }else{
                        if(d_getAlLEvents)  Log.e(Tag,"Size:" + list.size());

                        /** prepare our variables */
                        ParseObject  events;
                        ScheduleData[] allevents = new ScheduleData[list.size()];

                        for(int i=0; i<list.size();i++) {
                            events = list.get(i);
/*
                            Log.e(Tag, "fetchAllEvents()  " + events.getObjectId());
                            Log.e(Tag, "fetchAllEvents() "  +events.getDate(EVENT_TIME));
                            Log.e(Tag, "fetchAllEvents()  " +events.getString(EVENT_NAME));
                            Log.e(Tag, "fetchAllEvents()  " + events.getString(EVENT_LOCA));
                            Log.e(Tag, "fetchAllEvents()  " +  events.getString(EVENT_INSTR));
                            Log.e(Tag, "fetchAllEvents()  " + events.getString(EVENT_DESC));*/

                            ScheduleData detail      = new ScheduleData();
                            detail.event_Id          = events.getObjectId();
                            detail.event_date        = events.getDate(EVENT_TIME);
                            detail.event_name        = events.getString(EVENT_NAME);
                            detail.event_location    = events.getString(EVENT_LOCA);
                            detail.event_instructor  = events.getString(EVENT_INSTR);
                            detail.event_description = events.getString(EVENT_DESC);
                            detail.event_current_count = events.getInt(EVENT_CNT);
                            detail.event_max_count     = events.getInt(EVENT_MAX);

                            allevents[i] = detail;
                        }

                        callback.onSuccessEvents(allevents);
                    }
                }else{
                    callback.onFailEvents("Error @ 1st Stage");
                }
            }
        });
    }

    /** Books and event*/
    public static void BookEvent(final ScheduleData eventData, final int userId, final ParseBookEventCallback callback){

        if(d_bookClass)Log.e(Tag,"Booking with userIdd:" + userId + " eventId: " + eventData.event_Id );

        ParseQuery<ParseObject> attendQuery = ParseQuery.getQuery(CLASS_ATTEND);
        attendQuery.whereEqualTo(ATTEN_USERID,userId);
        attendQuery.whereMatches(ATTEN_EVENTID, eventData.event_Id);
        attendQuery.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> list, ParseException e) {
                if(e==null){
                    if(list.size() > 0){

                        callback.onBookFailed("Already booked!");
                    }else{
                        if(d_bookClass)Log.e(Tag,"Book Class Enter");
                        ParseQuery<ParseObject> eventQuery = ParseQuery.getQuery(CLASS_EVENTS);
                        eventQuery.whereMatches(OBJECT_ID, eventData.event_Id);
                        eventQuery.findInBackground(new FindCallback<ParseObject>() {

                            @Override
                            public void done(List<ParseObject> list, ParseException e) {
                                if (e == null) {
                                    if (list.size() != 1) {
                                        callback.onBookFailed("No Events Entries");
                                    } else {
                                        if (d_getAlLEvents) Log.e(Tag, "Size:" + list.size());

                                        /** get the event one more time and check if full again */
                                        ParseObject  event = list.get(0);
                                        int count = event.getInt(EVENT_CNT);
                                        int max   = event.getInt(EVENT_MAX);

                                        if(count < max){
                                            event.increment(EVENT_CNT);
                                            event.saveInBackground();

                                            ParseObject newAttendance = new ParseObject(CLASS_ATTEND);
                                            newAttendance.put(ATTEN_EVENTID, eventData.event_Id);
                                            newAttendance.put(ATTEN_USERID,  userId);
                                            newAttendance.put(ATTEN_TIME,    eventData.event_date);
                                            newAttendance.put(ATTEN_NAME, eventData.event_name);
                                            newAttendance.put(ATTEN_LOCA, eventData.event_location);
                                            newAttendance.saveInBackground();

                                            callback.onBookSuccessful("Book Successful! See you there!");

                                        }else{
                                            callback.onBookFailed("Class is full! ");
                                        }

                                    }
                                } else {
                                    callback.onBookFailed("Error @ 1st Stage");
                                }
                            }
                        });


                    }

                }
            }
        });
    }

    public static void CancelBooking(final ScheduleData attendanceData, final int userId, final ParseCancelbookingCallback callback){

        ParseQuery<ParseObject> eventQuery = ParseQuery.getQuery(CLASS_EVENTS);
        eventQuery.whereMatches(OBJECT_ID, attendanceData.event_Id);
        eventQuery.findInBackground(new FindCallback<ParseObject>() {

            @Override
            public void done(List<ParseObject> list, ParseException e) {
                if (e == null) {
                    if (list.size() != 1) {
                        callback.onCancelFail("No Events Entries");
                    } else {
                        if (d_getAlLEvents) Log.e(Tag, "Size:" + list.size());

                        /* decrement the value by one */
                        ParseObject event = list.get(0);
                        int count = event.getInt(EVENT_CNT);
                        event.put(EVENT_CNT, count - 1);
                        event.saveInBackground();

                        ParseQuery<ParseObject> attendanceQuery = ParseQuery.getQuery(CLASS_ATTEND);
                        attendanceQuery.whereMatches(OBJECT_ID, attendanceData.attedance_objectId);
                        attendanceQuery.findInBackground(new FindCallback<ParseObject>() {
                            @Override
                            public void done(List<ParseObject> list, ParseException e) {
                                if (e == null) {

                                    if (list.size() != 1) {
                                        callback.onCancelFail("No Events Entries");
                                    }else{
                                        ParseObject attendance = list.get(0);
                                        attendance.deleteInBackground();
                                        callback.onCancelSuccess();
                                    }
                                }else{
                                    callback.onCancelFail("Error @ 2st Stage");
                                }
                            }
                        });

                    }
                }else{
                    callback.onCancelFail("Error @ 1st Stage");
                }
                //GOOD
            }
        });
    }
}
