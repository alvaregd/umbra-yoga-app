package com.umbrayoga.umbrayoga.network;

/**
 * Created by alvaregd on 09/06/15.
 */
public interface ParseBookEventCallback {

    void onBookSuccessful(String message);
    void onBookFailed(String message);

}
