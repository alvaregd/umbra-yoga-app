package com.umbrayoga.umbrayoga.network;

import com.umbrayoga.umbrayoga.data.StudioData;

/**
 * Created by alvaregd on 08/06/15.
 */
public interface ParseFetchStudioCallback {

    void onSuccessStudio(StudioData[] data);
    void onFailStudio(String error);

}
