package com.umbrayoga.umbrayoga.network;

import com.umbrayoga.umbrayoga.data.ActivityData;


public interface ParseFetchActivityCallback {

    void onSuccessActivity(ActivityData[]  data);
    void onFailActivity(String error);

}
