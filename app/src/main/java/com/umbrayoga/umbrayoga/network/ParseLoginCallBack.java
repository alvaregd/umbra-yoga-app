package com.umbrayoga.umbrayoga.network;

/**
 * Created by alvaregd on 06/06/15.
 */
public interface ParseLoginCallBack {

    void onLoginSuccess(int id, String objId, String realname, String email, int score);
    void onLoginFail(String error);
}
