package com.umbrayoga.umbrayoga.network;

import com.umbrayoga.umbrayoga.data.ScheduleData;


public interface ParseEventDetailCallback {

    void onSuccess(ScheduleData detail);
    void onFail(String error);

}
