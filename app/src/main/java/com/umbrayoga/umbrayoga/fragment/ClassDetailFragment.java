package com.umbrayoga.umbrayoga.fragment;

import android.app.Fragment;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.umbrayoga.umbrayoga.R;
import com.umbrayoga.umbrayoga.callbacks.ClassDetailListener;
import com.umbrayoga.umbrayoga.callbacks.DialogCallback;
import com.umbrayoga.umbrayoga.data.ScheduleData;
import com.umbrayoga.umbrayoga.network.Parse;
import com.umbrayoga.umbrayoga.network.ParseEventDetailCallback;
import com.umbrayoga.umbrayoga.utils.DateUtils;
/**
 * Shows Detail a particular Class
 */


public class ClassDetailFragment extends Fragment implements ParseEventDetailCallback{

    private final static String Tag = "DetailFragment";
    View rootView;
    Button bCancel, bGood;
    TextView tvTitle, tvActivity, tvTime, tvInstructor, tvDescription;
    LinearLayout llButtons, llDescription, llDetails;
    ProgressBar bar;

    ClassDetailListener mListener;

    ScheduleData attendanceData;
    ScheduleData eventDetail;

    DialogCallback mDialogCallback= new DialogCallback() {

        @Override
        public void OnYesClick() {
            mListener.OnClassCancelled(attendanceData);
        }
    };

    public ClassDetailFragment(){
    }

    public void injectData(ScheduleData data){
        this.attendanceData = data;
    }

    public static ClassDetailFragment newInstance(int num, ClassDetailListener listener, ScheduleData attendanceData){
        ClassDetailFragment fragment = new ClassDetailFragment();
        fragment.injectListener(listener);
        fragment.injectData(attendanceData);
        Bundle args = new Bundle();
        args.putInt("num", num);
        fragment.setArguments(args);
        return fragment;
    }

    public void injectListener(ClassDetailListener listener){
        this.mListener = listener;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if(rootView == null){
            rootView = inflater.inflate(R.layout.fragment_class_detail,container,false);
        }

        tvTitle = (TextView)rootView.findViewById(R.id.tv_detail_title);
        tvActivity = (TextView)rootView.findViewById(R.id.tv_detail_activity);
        tvInstructor = (TextView)rootView.findViewById(R.id.tv_detail_instructor);
        tvTime = (TextView)rootView.findViewById(R.id.tv_detail_time);
        tvDescription = (TextView)rootView.findViewById(R.id.tv_detail_description);

        llButtons = (LinearLayout)rootView.findViewById(R.id.ll_detail_buttons);
        llDescription = (LinearLayout)rootView.findViewById(R.id.ll_detail_description);
        llDetails = (LinearLayout)rootView.findViewById(R.id.ll_short_detail);

        bar =(ProgressBar)rootView.findViewById(R.id.pb_fetching);

        bCancel = (Button)rootView.findViewById(R.id.b_cancel_class);
        bCancel.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                CancelPromptDialog frag = CancelPromptDialog.newInstance(0,mDialogCallback);
                FragmentTransaction ft =  getActivity().getFragmentManager().beginTransaction();
                frag.show(ft, "dialog");
            }
        });

        bGood = (Button)rootView.findViewById(R.id.b_ok_class);
        bGood.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.OnAgreePressed();

            }
        });

        Parse.LoadEventDetail(attendanceData.event_Id, this);
        return rootView;
    }

    @Override
    public void onSuccess(ScheduleData detail) {

        Log.e(Tag, "Event Details:" + detail.event_name);

        this.eventDetail = detail;

        tvTitle.setText(detail.event_name);
        tvDescription.setText(detail.event_description);
        tvTime.setText(DateUtils.convertToString(detail.event_date));
        tvInstructor.setText(detail.event_instructor);

        llButtons.setVisibility(View.VISIBLE);
        llDescription.setVisibility(View.VISIBLE);
        llDetails.setVisibility(View.VISIBLE);
        tvTitle.setVisibility(View.VISIBLE);

        bar.setVisibility(View.GONE);

    /*    tvTitle.setVisibility(View.VISIBLE);
        tvDescription.setVisibility(View.VISIBLE);
        tvInstructor.setVisibility(View.VISIBLE);
        tvTime.setVisibility(View.VISIBLE);*/

    }

    @Override
    public void onFail(String error) {
        Log.e(Tag,"Event LOAD FAIL"+ error);

    }
}
