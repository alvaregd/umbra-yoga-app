package com.umbrayoga.umbrayoga.fragment;

import android.app.DialogFragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.umbrayoga.umbrayoga.R;
import com.umbrayoga.umbrayoga.callbacks.DialogCallback;

public class CancelPromptDialog extends DialogFragment {

    private View rootView;

    private DialogCallback mCallback;

    public CancelPromptDialog(){

    }

    public void injectCallback(DialogCallback callback){
        this.mCallback = callback;
    }

    public static CancelPromptDialog newInstance(int num, DialogCallback callback){
        CancelPromptDialog fragment = new CancelPromptDialog();
        fragment.injectCallback(callback);
        Bundle args = new Bundle();
        args.putInt("num", num);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(android.support.v4.app.DialogFragment.STYLE_NO_TITLE, android.R.style.Theme_Holo_Dialog);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if(rootView== null){
           rootView = inflater.inflate(R.layout.fragment_cancel_dialog, container,false);
        }

        rootView.findViewById(R.id.b_dialog_no).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               dismiss();
            }
        });

        rootView.findViewById(R.id.b_dialog_yes).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCallback.OnYesClick();
                dismiss();
            }
        });

        return rootView;
    }
}
