package com.umbrayoga.umbrayoga.fragment;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.TextView;

import com.umbrayoga.umbrayoga.R;
import com.umbrayoga.umbrayoga.activity.BookFlowActivity;
import com.umbrayoga.umbrayoga.adapter.ScheduleListAdapter;
import com.umbrayoga.umbrayoga.application.App;
import com.umbrayoga.umbrayoga.callbacks.ListItemClickListener;
import com.umbrayoga.umbrayoga.data.ScheduleData;
import com.umbrayoga.umbrayoga.network.Parse;
import com.umbrayoga.umbrayoga.network.ParseFetchEventsCallback;

public class ScheduleListFragment extends Fragment implements AbsListView.OnItemClickListener,
        ParseFetchEventsCallback{

    private final static String Tag = "ScheduleListFragment";
    private final static boolean d_onCreateView = true;
    private final static boolean d_onResume = true;

    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private String mParam1;
    private String mParam2;

    private View rootView;
    private ListItemClickListener mListener;
    private AbsListView mListView;
    private ScheduleListAdapter mAdapter;
    private View emptyMessage;

    private Typeface font;

    public ScheduleListFragment() {

    }

    // TODO: Rename and change types of parameters
    public static ScheduleListFragment newInstance(String param1, String param2) {
        ScheduleListFragment fragment = new ScheduleListFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }

        mAdapter = new ScheduleListAdapter(getActivity());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if(d_onCreateView) Log.e(Tag,"onCreateView() Enter");

        if(rootView == null){
            rootView = inflater.inflate(R.layout.fragment_schedule_list, container, false);
        }

        mListView = (AbsListView) rootView.findViewById(android.R.id.list);
        mListView.setAdapter(mAdapter);
        mListView.setOnItemClickListener(this);

        rootView.findViewById(R.id.b_Book).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity().getApplicationContext(), BookFlowActivity.class));
            }
        });

        emptyMessage = rootView.findViewById(R.id.ll_empty_message);

        font = Typeface.createFromAsset(getActivity().getAssets(), "EraserDust.ttf");
        ((TextView)rootView.findViewById(R.id.tv_title)).setTypeface(font);
        ((TextView)rootView.findViewById(R.id.tv_empty_message)).setTypeface(font);

        return rootView;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (ListItemClickListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onResume() {
        if(d_onResume)Log.e(Tag,"onResume() Enter");
        super.onResume();
        mAdapter.clear();
        int userId = ((App)getActivity().getApplication()).getUserId();
        if( userId!= -1){
            Parse.fetchMyEvents(userId, this);
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        if (null != mListener) {
                mListener.OnClassClicked(position, mAdapter.getItem(position));
        }
    }

    public void CheckListEmpty(){
        emptyMessage.setVisibility((mAdapter.getCount() == 0)? View.VISIBLE:View.GONE);
    }

    @Override
    public void OnFetchEventReceived(ScheduleData[] data) {
        mAdapter.clear();
        mAdapter.addAll(data);
        ((App)getActivity().getApplication()).setMyEvents(data);
        CheckListEmpty();
    }

    @Override
    public void OnError(String descrption) {

    }



}
