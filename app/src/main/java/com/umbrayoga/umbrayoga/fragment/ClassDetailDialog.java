package com.umbrayoga.umbrayoga.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.umbrayoga.umbrayoga.R;
import com.umbrayoga.umbrayoga.activity.BaseActivity;
import com.umbrayoga.umbrayoga.application.App;
import com.umbrayoga.umbrayoga.callbacks.BookListener;
import com.umbrayoga.umbrayoga.data.ScheduleData;
import com.umbrayoga.umbrayoga.network.Parse;
import com.umbrayoga.umbrayoga.network.ParseBookEventCallback;
import com.umbrayoga.umbrayoga.utils.DateUtils;


public class ClassDetailDialog extends DialogFragment implements View.OnClickListener, ParseBookEventCallback {

    private final static String Tag = "LoginDialog";
    private View rootView;
    Button bBook;
    TextView tvTitle, tvActivity, tvTime, tvInstructor, tvDescription;
    LinearLayout llButtons, llDescription, llDetails;
    LinearLayout llBook;
    ProgressBar bar;

    ScheduleData data;

    public BookListener listener;

    public void injectData(ScheduleData data){
        this.data = data;
    }

    public static ClassDetailDialog newInstance(int num, ScheduleData data, BookListener listener){
        ClassDetailDialog fragment = new ClassDetailDialog();
        fragment.listener = listener;
        fragment.injectData(data);
        Bundle args = new Bundle();
        args.putInt("num", num);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(android.support.v4.app.DialogFragment.STYLE_NO_TITLE, android.R.style.Theme_Holo_Dialog);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if(rootView== null) {
            rootView = inflater.inflate(R.layout.fragment_class_detail, container, false);
        }


        tvTitle = (TextView)rootView.findViewById(R.id.tv_detail_title);
        tvActivity = (TextView)rootView.findViewById(R.id.tv_detail_activity);
        tvInstructor = (TextView)rootView.findViewById(R.id.tv_detail_instructor);
        tvTime = (TextView)rootView.findViewById(R.id.tv_detail_time);
        tvDescription = (TextView)rootView.findViewById(R.id.tv_detail_description);

        bBook = (Button)rootView.findViewById(R.id.b_book_now);
        bBook.setOnClickListener(this);


        llButtons = (LinearLayout)rootView.findViewById(R.id.ll_detail_buttons);
        llDescription = (LinearLayout)rootView.findViewById(R.id.ll_detail_description);
        llDetails = (LinearLayout)rootView.findViewById(R.id.ll_short_detail);
        llBook = (LinearLayout)rootView.findViewById(R.id.ll_book);

        bar =(ProgressBar)rootView.findViewById(R.id.pb_fetching);

        if(data != null){

            tvTitle.setText(data.event_name);
            tvDescription.setText(data.event_description);
            tvTime.setText(DateUtils.convertToString(data.event_date));
            tvInstructor.setText(data.event_instructor);

            llDescription.setVisibility(View.VISIBLE);
            llDetails.setVisibility(View.VISIBLE);
            tvTitle.setVisibility(View.VISIBLE);
            llBook.setVisibility(View.VISIBLE);

            bar.setVisibility(View.GONE);

            if(data.event_current_count < data.event_max_count){
                bBook.setEnabled(true);
            }else{
                bBook.setEnabled(true);
            }
        }
        return rootView;
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){

            case R.id.b_book_now:
                if(!((App)getActivity().getApplication()).isLoggedIn()){

                    ((BaseActivity)getActivity()).ShowLoginDialog();
                    Toast.makeText(getActivity(),"Log in to book this class", Toast.LENGTH_SHORT).show();
                }else{
                    if(data.event_current_count < data.event_max_count){
                        Parse.BookEvent(data,
                                ((App)getActivity().getApplication()).getUserId(),
                                this);
                    }else{
                        Toast.makeText(getActivity(),"Class is full! :(", Toast.LENGTH_SHORT).show();
                    }
                }
                break;
        }
    }

    @Override
    public void onBookSuccessful(String message) {
        dismiss();
        Toast.makeText(getActivity(), "Book Successful! :)", Toast.LENGTH_SHORT).show();
        listener.IncrementBookEvent();
    }

    @Override
    public void onBookFailed(String message) {
        Toast.makeText(getActivity(),message, Toast.LENGTH_SHORT).show();
    }
}