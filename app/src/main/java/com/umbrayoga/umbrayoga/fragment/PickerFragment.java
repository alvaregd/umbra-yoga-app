package com.umbrayoga.umbrayoga.fragment;

import android.app.Activity;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ExpandableListView;
import android.widget.FrameLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.umbrayoga.umbrayoga.R;
import com.umbrayoga.umbrayoga.activity.BaseActivity;
import com.umbrayoga.umbrayoga.activity.BookFlowActivity;
import com.umbrayoga.umbrayoga.adapter.ActivityListAdapter;
import com.umbrayoga.umbrayoga.adapter.EventsListAdapter;
import com.umbrayoga.umbrayoga.adapter.StudioListAdapter;
import com.umbrayoga.umbrayoga.callbacks.AccountChangedListener;
import com.umbrayoga.umbrayoga.callbacks.BookFlowCallbacks;
import com.umbrayoga.umbrayoga.callbacks.BookListener;
import com.umbrayoga.umbrayoga.data.ActivityData;
import com.umbrayoga.umbrayoga.data.ScheduleData;
import com.umbrayoga.umbrayoga.data.StudioData;
import com.umbrayoga.umbrayoga.network.Parse;
import com.umbrayoga.umbrayoga.network.ParseFetchActivityCallback;
import com.umbrayoga.umbrayoga.network.ParseFetchFutureEventsCallback;
import com.umbrayoga.umbrayoga.network.ParseFetchStudioCallback;


/**
 * A placeholder fragment containing a simple view.
 */
public class PickerFragment extends Fragment implements AdapterView.OnItemClickListener,ParseFetchStudioCallback,
        ParseFetchActivityCallback, ParseFetchFutureEventsCallback, BookListener, AccountChangedListener{

    private final static String  Tag = "PickerFragment";
    private final static boolean d_onAttach = true;
    private final static boolean d_onCreateView = true;

    private int selectionType;
    private static final String ARG_SECTION_NUMBER = "section_number";

    public final static int STUDIO   = 0;
    public final static int ACTIVITY = 1;
    public final static int TIME     = 2;

    private BookFlowActivity act;
    private ClassDetailDialog bookDialog;

    FrameLayout llemptyMessage;

    private int selectedGroup;
    private int selectedChild;

    View rootView;

    private ArrayAdapter mAdapter;
    private EventsListAdapter mExpandableAdapter;

    private BookListener listener;

    private void setSelectionType(int type){
        this.selectionType = type;
    }

    public static PickerFragment newInstance(int sectionNumber) {

        PickerFragment fragment = new PickerFragment();
        fragment.setSelectionType(sectionNumber);
        Bundle args = new Bundle();
        args.putInt(ARG_SECTION_NUMBER, sectionNumber);
        fragment.setArguments(args);
        return fragment;
    }

    public PickerFragment() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if(d_onCreateView)Log.e(Tag,"onCreateView()");

        if(rootView == null){
            switch (selectionType){
                case TIME: rootView = inflater.inflate(R.layout.fragment_expandable_list, container, false);break;
                default:  rootView = inflater.inflate(R.layout.fragment_picker_list, container, false);break;
            }
        }
        listener = this;
        /**Fill the ListView based on selectionEntry && based on previous value */
        llemptyMessage = (FrameLayout)rootView.findViewById(R.id.ll_empty_message);


        /**Set the Instruction */
        TextView instruction =(TextView) rootView.findViewById(R.id.tv_instruction);
        switch (selectionType){
            case 0:instruction.setText(getActivity().getResources().getText(R.string.pick_studio));break;
            case 1:instruction.setText(getActivity().getResources().getText(R.string.pick_activity));break;
            case 2:instruction.setText(getActivity().getResources().getText(R.string.pick_time));break;
        }

        /** Fill ListView COntent*/
        switch (selectionType) {
            case STUDIO:
                ListView list = (ListView)rootView.findViewById(R.id.normalList);
                list.setOnItemClickListener(this);
                mAdapter = new StudioListAdapter(getActivity());
                ((StudioListAdapter) mAdapter).setSelectedEntry(-1);
                llemptyMessage.setVisibility(View.GONE);

                if (((BookFlowActivity) getActivity()).getStudioData() == null) {
                    FetchData();
                } else {
                    mAdapter.addAll(((BookFlowActivity) getActivity()).getStudioData());
                    ((StudioListAdapter) mAdapter).setSelectedEntry(((BookFlowActivity) getActivity()).getQueryParameters().selectedStudioIndex);
                }
                list.setAdapter(mAdapter);
                break;

            case ACTIVITY:
                ListView list2 = (ListView)rootView.findViewById(R.id.normalList);
                list2.setOnItemClickListener(this);
                mAdapter = new ActivityListAdapter(getActivity());
                ((ActivityListAdapter) mAdapter).setSelectedEntry(-1);
                llemptyMessage.setVisibility(View.GONE);

                if (((BookFlowActivity) getActivity()).getStudioData() == null) {
                    FetchData();
                } else {
                    mAdapter.clear();
                    mAdapter.addAll(((BookFlowActivity) getActivity()).getActivityDatas());
                    ((ActivityListAdapter) mAdapter).setSelectedEntry(((BookFlowActivity) getActivity()).getQueryParameters().selectedActivityIndex);
                }
                list2.setAdapter(mAdapter);
                break;
            case TIME:

                /** make the adapter and set its selected entry to none */
                ExpandableListView listview = (ExpandableListView)rootView.findViewById(R.id.eList);
                mExpandableAdapter = new EventsListAdapter(getActivity());
                mExpandableAdapter.setSelectedEntry(-1);

                listview.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {

                    @Override
                    public boolean onChildClick(ExpandableListView parent, View v,
                                                int groupPosition, int childPosition, long id) {

                        selectedChild = childPosition;
                        selectedGroup = groupPosition;

                        bookDialog = ClassDetailDialog.newInstance(0,
                                (ScheduleData)mExpandableAdapter.getChild(groupPosition,childPosition),
                                listener);
                        FragmentTransaction ft =  getFragmentManager().beginTransaction();
                        bookDialog.show(ft, "yo");
                        return false;
                    }
                });

                /** did the user select any search criterias in the previous fragments? */
                if (((BookFlowActivity) getActivity()).getQueryParameters().selectedActivityIndex == -1 &&
                        ((BookFlowActivity) getActivity()).getQueryParameters().selectedStudioIndex == -1) {
                    /** no so tell the user to pick out criterias */
                    llemptyMessage.setVisibility(View.VISIBLE);

                } else {
                    llemptyMessage.setVisibility(View.GONE);
                    Parse.fetchAllEvents(this);
                }

                listview.setAdapter(mExpandableAdapter);
                break;
        }

        return rootView;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        switch (selectionType){

            case STUDIO:
                ((StudioListAdapter)mAdapter).setSelectedEntry(position);
                mAdapter.notifyDataSetChanged();
                ((BookFlowCallbacks)getActivity()).onStudioSelected(((StudioListAdapter) mAdapter).getItem(position).objectId,position);
                break;

            case ACTIVITY:
                ((ActivityListAdapter)mAdapter).setSelectedEntry(position);
                mAdapter.notifyDataSetChanged();
                ((BookFlowCallbacks)getActivity()).onActivitySelected(((ActivityListAdapter) mAdapter).getItem(position).objectId,position);
                break;

            case TIME:
//                ((BookFlowCallbacks)getActivity()).onTimeSelected(position);
                break;
        }
    }

    @Override
    public void onAttach(Activity activity) {

        if(d_onAttach)Log.e(Tag,"onAttach");
        super.onAttach(activity);
        this.act = (BookFlowActivity)activity;
        ((BaseActivity)activity).injectListener(this);
    }

    private void FetchData(){
        if(selectionType == STUDIO){
            act.showProgressBar();
            Parse.FetchStudio(this);
        }else if(selectionType ==ACTIVITY){
            act.showProgressBar();
            Parse.FetchActivity(this);
        }
    }

    /** Network callbacks */
    @Override
    public void onSuccessActivity(ActivityData[] data) {

        Log.e(Tag, "onSuccessActivity() Size " + data.length);
        act.hidePRogressBar();
        act.setActivityDatas(data);
        mAdapter.addAll(data);
    }

    @Override
    public void onFailActivity(String error) {

    }

    @Override
    public void onSuccessEvents(ScheduleData[] data) {
        Log.e(Tag, "onSuccessEvents() Size " + data.length);

     /*   Log.e(Tag, "onSuccessEvents()  " + data[0].event_Id);
        Log.e(Tag, "onSuccessEvents() "  + data[0].event_name);
        Log.e(Tag, "onSuccessEvents()  " + data[0].event_date);
        Log.e(Tag, "onSuccessEvents()  " + data[0].event_description);
        Log.e(Tag, "onSuccessEvents()  " + data[0].event_location);
        Log.e(Tag, "onSuccessEvents()  " + data[0].event_instructor);
*/
        act.hidePRogressBar();
        act.setScheduleData(data);
        mExpandableAdapter.addAll(data);
        mExpandableAdapter.notifyDataSetChanged();

    }

    @Override
    public void onFailEvents(String error) {
    }

    @Override
    public void onSuccessStudio(StudioData[] data) {

        Log.e(Tag, "OnSucessStudio() Size " + data.length);
        act.hidePRogressBar();
        act.setStudioData(data);
        mAdapter.addAll(data);
    }

    @Override
    public void onFailStudio(String error) {
        Toast.makeText(getActivity(),"Check Internet Connection", Toast.LENGTH_LONG).show();
    }

    @Override
    public void IncrementBookEvent() {

        if(selectedGroup != -1 || selectedChild != -1){
            mExpandableAdapter.UpdateCounter(selectedGroup,selectedChild);
            selectedGroup = -1;
            selectedChild = -1;
        }
    }

    @Override
    public void OnAccountStateChanged(int State) {

    }
}