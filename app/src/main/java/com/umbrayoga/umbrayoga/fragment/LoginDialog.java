package com.umbrayoga.umbrayoga.fragment;

import android.app.Activity;
import android.app.DialogFragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.umbrayoga.umbrayoga.R;
import com.umbrayoga.umbrayoga.application.App;
import com.umbrayoga.umbrayoga.callbacks.DialogDismissListener;
import com.umbrayoga.umbrayoga.data.ParseNewUserEntry;
import com.umbrayoga.umbrayoga.network.Parse;
import com.umbrayoga.umbrayoga.network.ParseLoginCallBack;
import com.umbrayoga.umbrayoga.statics.Keys;
import com.umbrayoga.umbrayoga.utils.ValidationUtils;

public class LoginDialog extends DialogFragment {

    private final static String Tag = "LoginDialog";

    private View rootView;
    private ParseLoginCallBack mCallback;

    private Button login;
    private EditText etUsername, etPassword, etRealName;
    private TextView tvNewAccount;
    private View space;
    private ProgressBar bar;

    private DialogDismissListener mDismisListener;

    private boolean isSigningUp = false;

    public LoginDialog(){
    }

    public void injectListeners(DialogDismissListener dismissListener){
        this.mDismisListener = dismissListener;
    }
    public void injectCallback(ParseLoginCallBack callback){
        this.mCallback = callback;
    }

    public static LoginDialog newInstance(int num, ParseLoginCallBack callback,DialogDismissListener dismissListener ){

        LoginDialog fragment = new LoginDialog();
        fragment.injectCallback(callback);
        fragment.injectListeners(dismissListener);
        Bundle args = new Bundle();
        args.putInt("num", num);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(android.support.v4.app.DialogFragment.STYLE_NO_TITLE, android.R.style.Theme_Holo_Dialog);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if(rootView== null) {
            rootView = inflater.inflate(R.layout.fragmen_login, container, false);
        }

        etPassword = (EditText)rootView.findViewById(R.id.et_password);
        etUsername = (EditText)rootView.findViewById(R.id.et_username);

        login = (Button)rootView.findViewById(R.id.b_login);
        login.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                if(isSigningUp){
                    DisableUI();
                   if(ValidationUtils.checkCredentials(etUsername,etPassword,etRealName)){

                       Log.e(Tag,"validation OK ");
                       Parse.createNewUser(
                               new ParseNewUserEntry(
                                       etUsername.getText().toString(),
                                       etPassword.getText().toString(),
                                       etRealName.getText().toString(),
                                       getActivity().getSharedPreferences(
                                               Keys.ACCOUNT_PREFERENCES, Activity.MODE_PRIVATE)
                                               .getInt(Keys.USR_SCORE,0)
                               ),
                               (App)getActivity().getApplication()
                       );
                   }else{
                       ReEnableUI("Woops, there's a mistake");
                   }
                }else {

                    DisableUI();
                    if(ValidationUtils.checkLoginCredentials(etUsername,etPassword)){
                        Parse.ReLogin(
                                new String[]{etUsername.getText().toString(), etPassword.getText().toString()},
                                mCallback);
                    }else{
                        ReEnableUI("Woops, there's a mistake");
                    }
                }
            }
        });

        etRealName = (EditText)rootView.findViewById(R.id.et_realname);
        space = rootView.findViewById(R.id.space_realname);

        tvNewAccount = (TextView)rootView.findViewById(R.id.tv_newAccount);
        tvNewAccount.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                etRealName.setVisibility(View.VISIBLE);
                space.setVisibility(View.VISIBLE);
//                tvNewAccount.setVisibility(View.GONE);
                tvNewAccount.setText(getResources().getString(R.string.agreement_text));

                rootView.requestLayout();
                isSigningUp = true;
                etRealName.requestFocus();
                login.setText("Sign me up!");

            }
        });

        bar = (ProgressBar)rootView.findViewById(R.id.pg_loadig);

        return rootView;
    }

    public void ReEnableUI(String error){
        bar.setVisibility(View.GONE);
        login.setEnabled(true);
        etPassword.setEnabled(true);
        etUsername.setEnabled(true);
        etRealName.setEnabled(true);
        Toast.makeText(getActivity(), error, Toast.LENGTH_LONG).show();
        rootView.requestLayout();
    }

    public void DisableUI(){

        bar.setVisibility(View.VISIBLE);
        login.setEnabled(false);
        etPassword.setEnabled(false);
        etUsername.setEnabled(false);
        etRealName.setEnabled(false);
        rootView.requestLayout();

    }

    @Override
    public void dismiss() {
        mDismisListener.onDismiss();
        super.dismiss();
    }
}
