package com.umbrayoga.umbrayoga.fragment;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.net.Uri;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.format.Time;
import android.text.style.UnderlineSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.umbrayoga.umbrayoga.R;
import com.umbrayoga.umbrayoga.activity.BaseActivity;
import com.umbrayoga.umbrayoga.activity.SchedulesActivity;
import com.umbrayoga.umbrayoga.application.App;
import com.umbrayoga.umbrayoga.callbacks.AccountChangedListener;
import com.umbrayoga.umbrayoga.callbacks.GameListener;
import com.umbrayoga.umbrayoga.data.ScheduleData;
import com.umbrayoga.umbrayoga.network.Parse;
import com.umbrayoga.umbrayoga.network.ParseFetchEarliestEventCallback;
import com.umbrayoga.umbrayoga.statics.Keys;
import com.umbrayoga.umbrayoga.utils.DateUtils;
import com.umbrayoga.umbrayoga.view.BalanceSurfaceView;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedList;

/**
 * A placeholder fragment containing a simple view.
 */
public class MainScreenFragment extends Fragment implements SensorEventListener, AccountChangedListener,
        ParseFetchEarliestEventCallback{

    private final static String Tag  = "aiFragment";
    private final static boolean d_onResume = true;

    private ScheduleData earliestEvent;

    /** interface */
    View rootView;
    Button bSchedule;
    BalanceSurfaceView gameSurface;
    TextView tvName, tvNextClass;
    TextView tvTitle, tvDate,tvTime;
    TextView tvMap;

    private static SensorManager sm;
    private static Sensor sensor;
    float sensorZ;

    /** Filter for our sensor */
    static LinkedList<Float> filter;
    static float filterAverage = 0;
    static int filterIndex = 0;
    final static int FILTER_LENGTH = 10;

    private Calendar cal;
    private String dateString;
    private String timeString;
    private String locationString;

    GameListener mGameListener = new GameListener() {

        @Override
        public void SaveHighScore(int highscore) {

            if(((App)getActivity().getApplication()).isLoggedIn()){
                SharedPreferences prefs = getActivity().getSharedPreferences(Keys.ACCOUNT_PREFERENCES, Activity.MODE_PRIVATE);
                SharedPreferences.Editor edit = prefs.edit();
                edit.putInt(Keys.USR_SCORE,highscore);
                edit.apply();
            }
        }

        @Override
        public int getHighScore() {

            if(((App)getActivity().getApplication()).isLoggedIn()){
                SharedPreferences prefs = getActivity().getSharedPreferences(Keys.ACCOUNT_PREFERENCES, Activity.MODE_PRIVATE);
                return prefs.getInt(Keys.USR_SCORE,0);
            }else{
                return 0;
            }

        }
    };

    public MainScreenFragment() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if(rootView == null ){
            rootView =  inflater.inflate(R.layout.fragment_main_screen, container, false);
        }

        /** prepare the UI*/
        tvMap= (TextView)rootView.findViewById(R.id.tv_map);
        Spannable content = new SpannableString("See You There!");
        content.setSpan(new UnderlineSpan(), 0, content.length(), 0);
        tvMap.setText(content);
        tvMap.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Uri gmmIntentUri = Uri.parse("geo:0,0?q="+locationString);
                Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
                mapIntent.setPackage("com.google.android.apps.maps");
                startActivity(mapIntent);
            }
        });

        tvName      =(TextView)rootView.findViewById(R.id.tv_main_name);
        tvNextClass =(TextView)rootView.findViewById(R.id.tv_next_class);
        tvTitle     =(TextView)rootView.findViewById(R.id.tv_main_even_name);
        tvDate      =(TextView)rootView.findViewById(R.id.tv_main_date);
        tvTime      =(TextView)rootView.findViewById(R.id.tv_main_time);

        Resources res = getResources();
        gameSurface = (BalanceSurfaceView)rootView.findViewById(R.id.surface);
        gameSurface.setZOrderOnTop(true);
        gameSurface.injectListener(mGameListener);
        gameSurface.injectAssets(new Bitmap[]{
                BitmapFactory.decodeResource(res, R.drawable.pose_1),
                BitmapFactory.decodeResource(res, R.drawable.pose_2),
                BitmapFactory.decodeResource(res, R.drawable.pose_3),
                BitmapFactory.decodeResource(res, R.drawable.pose_4),
                BitmapFactory.decodeResource(res, R.drawable.pose_5),
                BitmapFactory.decodeResource(res, R.drawable.pose_6)
        });
        gameSurface.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                gameSurface.NextState();
            }
        });

        bSchedule = (Button)rootView.findViewById(R.id.b_schedule);
        bSchedule.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity( new Intent(getActivity().getApplicationContext(),SchedulesActivity.class));
            }
        });

        /** prepare the sensor */
        sm = (SensorManager) getActivity().getSystemService(Context.SENSOR_SERVICE);
        if (sm.getSensorList(Sensor.TYPE_GYROSCOPE).size() != 0) {
            sensor = sm.getSensorList(Sensor.TYPE_GYROSCOPE).get(0);
            sm.registerListener(this, sensor, SensorManager.SENSOR_DELAY_GAME);
        }

        filter = new LinkedList<>();
        //instantiate filter
        for (int i = 0; i < FILTER_LENGTH; i++) {
            filter.add(i, 0.0f);
        }


        return rootView;
    }

    @Override
    public void onResume() {
        if(d_onResume) Log.e(Tag,"onResume() Enter");
        super.onResume();
        sm.registerListener(this, sensor, SensorManager.SENSOR_DELAY_GAME);
        gameSurface.resume();
        OnAccountStateChanged(0);
    }

    @Override
    public void onPause() {
        super.onPause();
        sm.unregisterListener(this);
        gameSurface.pause();
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        ((BaseActivity)activity).injectListener(this);
    }

    /** Sensor stuff  **/
    @Override
    public void onSensorChanged(SensorEvent sensorEvent) {

        try{
            Thread.sleep(16);
        }catch (InterruptedException e) {

        }
        sensorZ  = sensorEvent.values[1];

        filterAverage = 0;
        for(filterIndex  = FILTER_LENGTH -1 ;filterIndex > -1; filterIndex--){

            filterAverage = filterAverage + filter.get(filterIndex);
            if(filterIndex == 0){
                filter.set(filterIndex, sensorZ);
            }else{
                filter.set(filterIndex, filter.get(filterIndex-1));
            }
        }
//        Log.e("GYRO:", " Z: " + sensorEvent.values[2]);
        gameSurface.updateSensorValues(sensorEvent.values[2], filterAverage / FILTER_LENGTH);
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }

    @Override
    public void OnAccountStateChanged(int State) {
        populateForm();
        fetchNextEvent();
    }

    public void populateForm(){

        if(((App)getActivity().getApplication()).isLoggedIn()){
            /** wer are logged in, get our latest event */
            tvName.setText( "Hello, "+((App)getActivity().getApplication()).getName());
            tvNextClass.setText("Your next class is:");
        }
        else{
            /** we are not logged int, get the latest event ever */
            tvName.setText( "Welcome, ");
            tvNextClass.setText("Our next class is:");
        }
    }


    /************************************ Network Operations ********************************/
    public void fetchNextEvent(){

        if(((App)getActivity().getApplication()).isLoggedIn()){
            /** wer are logged in, get our latest event */
            Parse.fetchMyEarliestEvents(((App)getActivity().getApplicationContext()).getUserId(),
                    this);
            bSchedule.setText("My Schedule");
        }
        else{
            /** we are not logged int, get the latest event ever */
            Parse.fetchAnyEarliestEvents(this);
            bSchedule.setText("Book this class");

        }
    }

    @Override
    public void OnFetchEarliestSuccess(ScheduleData data) {

        tvMap.setVisibility(View.VISIBLE);

        dateString =
                ((DateUtils.dayDifference(new Date(), data.event_date)<7) ? "Next " : "")
                + android.text.format.DateFormat.format("EEEE", data.event_date)
                + ", "
                + DateUtils.MonthLongth(
                Integer.valueOf(
                        (String)android.text.format.DateFormat.format("MM", data.event_date)
                      ) - 1
                 )
                + " "
                +  android.text.format.DateFormat.format("dd", data.event_date);

        tvDate.setText(dateString);

        if(cal == null){
            cal = Calendar.getInstance();
        }

        cal.setTime(data.event_date);
        timeString =
                "At "
                        + cal.get(Calendar.HOUR)
                        + ":" + DateUtils.addZero(cal.get(Calendar.MINUTE))
                        + " "
                        +  DateUtils.AMPM(cal.get(Calendar.AM_PM));
        tvTime.setText(timeString);
        tvTitle.setText(data.event_name);

        /** Set location check time**/
        locationString  = data.event_location;

    }

    @Override
    public void OnFetchEarliestFail(String error) {

        tvNextClass.setText("Our Next class is:");
        bSchedule.setText("Book a class");
        Parse.fetchAnyEarliestEvents(this);

        Log.e(Tag,"Error:" + error);
    }
}
