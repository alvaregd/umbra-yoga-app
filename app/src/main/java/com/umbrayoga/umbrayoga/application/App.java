package com.umbrayoga.umbrayoga.application;

import android.app.Activity;
import android.content.SharedPreferences;
import android.util.Log;

import com.parse.Parse;
import com.umbrayoga.umbrayoga.callbacks.LoginCallBack;
import com.umbrayoga.umbrayoga.data.ScheduleData;
import com.umbrayoga.umbrayoga.network.ParseLoginCallBack;
import com.umbrayoga.umbrayoga.network.ParseNewUserCallback;
import com.umbrayoga.umbrayoga.statics.Keys;
import com.umbrayoga.umbrayoga.utils.ParseUtils;

import java.util.HashMap;

/**
 * Main Application class.
 * Manages the Account state
 */

public class App extends android.app.Application implements ParseLoginCallBack,
  ParseNewUserCallback {

    private final static String Tag = "App";
    private final static boolean d_NewAccountSuccess = true;
    private final static boolean d_newAccounError = true;


    public final static int STATE_LOGGEDIN = 0;
    public final static int STATE_LOGGEDOUT = 1;

    LoginCallBack mLoginCallBack;
    SharedPreferences prefs;

    public ScheduleData getMyEvent(String objectId) {
        return this.myEvents.get(objectId);
    }

    public void setMyEvents(ScheduleData[] myEvents) {

        this.myEvents.clear();
        for(ScheduleData event: myEvents){
            this.myEvents.put(event.attedance_objectId, event);

        }
    }
    HashMap<String, ScheduleData> myEvents = new HashMap<>();


    @Override
    public void onCreate() {
        super.onCreate();
        Parse.enableLocalDatastore(this);
        Parse.initialize(this, "baWqMKzzIGHYlOBp7UYSWZ07NdHWlsI9OvRl5wLq", "AAXHtkobwAMG44VFgN5XDgRYsx966cXyH7toCTr9");

        prefs = getSharedPreferences(Keys.ACCOUNT_PREFERENCES, Activity.MODE_PRIVATE);
    }

    /** Every activity will call this function to set its callback
     *  This way, when we return from an attempted login, we can
     *  update the correct UI
     */
    public void setmLoginCallBack(LoginCallBack mLoginCallBack){
        this.mLoginCallBack = mLoginCallBack;
    }

    /** Save the user information when login is successful*/
    @Override
    public void onLoginSuccess(int id,
                               String objId,
                               String realname,
                               String email,
                               int score
                                ) {

        SharedPreferences.Editor editor = prefs.edit();
        editor.putInt(Keys.USR_USERID, id);
        editor.putString(Keys.USR_OBJECTID, objId);
        editor.putString(Keys.USR_EMAIL,email);
        editor.putString(Keys.USR_NAME, realname);
        editor.putInt(Keys.USR_SCORE, score);
        editor.apply();

        if(mLoginCallBack != null){
            mLoginCallBack.onLoginSuccess();
        }
    }

    /** notify the activity that the login failed */
    @Override
    public void onLoginFail(String error) {
        mLoginCallBack.onLoginFail(error);
    }

    public void signOut(){

        SharedPreferences.Editor editor = prefs.edit();
        editor.putInt(Keys.USR_USERID, -1);
        editor.putString(Keys.USR_OBJECTID, "");
        editor.putString(Keys.USR_EMAIL,"");
        editor.putString(Keys.USR_NAME, "");
        editor.apply();

        if(mLoginCallBack != null){
            mLoginCallBack.onLogout();
        }
    }

    public boolean isLoggedIn(){
       return  prefs.getInt(Keys.USR_USERID, -1) != -1;
    }

    @Override
    public void NewUserSuccess(int id, String object, String name, String email) {

        if(d_NewAccountSuccess)Log.e(Tag,"Success Callback");

        /** save the user preferences */
        SharedPreferences.Editor editor = prefs.edit();
        editor.putInt(Keys.USR_USERID, id);
        editor.putString(Keys.USR_OBJECTID, object);
        editor.putString(Keys.USR_EMAIL,    email);
        editor.putString(Keys.USR_NAME, name);
        editor.apply();

        mLoginCallBack.onLoginSuccess();
    }

    @Override
    public void NewUserError(String error) {

        mLoginCallBack.onLoginFail(error);
        if(d_newAccounError)Log.e(Tag,"New Account Error: " + error);
    }

    public SharedPreferences getPreferences(){
        return this.prefs;
    }

    /***************************************** get stuff **************************************/
    public String getName(){
        String name = prefs.getString(Keys.USR_NAME,"");
        if(!name.isEmpty()) return ParseUtils.getFirstName(name);
        else return "";
    }
    public int getUserId(){
         return prefs.getInt(Keys.USR_USERID, -1);
    }

}
