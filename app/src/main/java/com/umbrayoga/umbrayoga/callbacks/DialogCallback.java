package com.umbrayoga.umbrayoga.callbacks;

/**
 * Created by alvaregd on 03/06/15.
 */
public interface DialogCallback {

    public void OnYesClick();
}
