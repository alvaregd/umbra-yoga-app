package com.umbrayoga.umbrayoga.callbacks;

import com.umbrayoga.umbrayoga.data.ScheduleData;

public interface FetchAllEventsCallback {

    void onSuccess(ScheduleData data);
    void onFail(String message);
}
