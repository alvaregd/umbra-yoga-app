package com.umbrayoga.umbrayoga.callbacks;

/**
 * Created by alvaregd on 06/06/15.
 */
public interface DialogDismissListener {

    void onDismiss();
}
