package com.umbrayoga.umbrayoga.callbacks;

import com.umbrayoga.umbrayoga.data.ScheduleData;

/**
 * Created by alvaregd on 03/06/15.
 */
public interface ListItemClickListener {

    void OnClassClicked(int id, ScheduleData attendanceData);
}
