package com.umbrayoga.umbrayoga.callbacks;
import com.umbrayoga.umbrayoga.data.ScheduleData;

public interface ClassDetailListener {
    void OnClassCancelled(ScheduleData data);
    void OnAgreePressed();

}
