package com.umbrayoga.umbrayoga.callbacks;


public interface BookFlowCallbacks {

    void onStudioSelected   (String studio, int selectedIndex);
    void onActivitySelected (String Activity, int selectedIndex );
    void onTimeSelected      (String time, int selectedIndex);



}
