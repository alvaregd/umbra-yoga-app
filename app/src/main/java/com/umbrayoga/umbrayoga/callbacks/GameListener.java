package com.umbrayoga.umbrayoga.callbacks;

/**
 * Created by alvaregd on 05/06/15.
 */
public interface GameListener {

    void SaveHighScore(int highscore);
    int getHighScore();

}
