package com.umbrayoga.umbrayoga.callbacks;

/**
 * Monitors the event when an account changed so we can change the UI
 * Accordingly (logged in or not )
 */
public interface AccountChangedListener {

    void OnAccountStateChanged(int State);

}
