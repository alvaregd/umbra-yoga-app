package com.umbrayoga.umbrayoga.callbacks;

/**
 * Created by alvaregd on 06/06/15.
 */
public interface LoginCallBack {

    void onLoginSuccess();
    void onLoginFail(String error);
    void onLogout();
}
