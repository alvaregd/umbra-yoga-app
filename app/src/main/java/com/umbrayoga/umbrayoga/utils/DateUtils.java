package com.umbrayoga.umbrayoga.utils;

import java.util.Calendar;
import java.util.Date;


public class DateUtils {
    public static String Month(int pos){
        switch(pos){
            case 0: return "Jan";
            case 1: return "Feb";
            case 2: return "Mar";
            case 3: return "Apr";
            case 4: return "May";
            case 5: return "Jun";
            case 6: return "Jul";
            case 7: return "Aug";
            case 8: return "Sep";
            case 9: return "Oct";
            case 10: return "Nov";
            case 11: return "Dec";
            default: return "Defaulted!";
        }
    }

    public static String MonthLongth(int pos){
        switch(pos){
            case 0: return "January";
            case 1: return "February";
            case 2: return "March";
            case 3: return "April";
            case 4: return "May";
            case 5: return "June";
            case 6: return "July";
            case 7: return "August";
            case 8: return "Septempber";
            case 9: return "October";
            case 10: return "November";
            case 11: return "December";
            default: return "Defaulted!";
        }
    }

    public static String stringify(Calendar calendar) {

        return MonthLongth(calendar.get(Calendar.MONTH))
                + " "
                + calendar.get(Calendar.DAY_OF_MONTH)
                + ", "
                + calendar.get(Calendar.YEAR)
                + " at "
                + calendar.get(Calendar.HOUR_OF_DAY)
                + ":"
                + calendar.get(Calendar.MINUTE);

    }

    public static long dayDifference(Date startDate, Date endDate){

        //milliseconds
        long different = endDate.getTime() - startDate.getTime();

        System.out.println("startDate : " + startDate);
        System.out.println("endDate : "+ endDate);
        System.out.println("different : " + different);

        long secondsInMilli = 1000;
        long minutesInMilli = secondsInMilli * 60;
        long hoursInMilli = minutesInMilli * 60;
        long daysInMilli = hoursInMilli * 24;

        long elapsedDays = different / daysInMilli;
        different = different % daysInMilli;

        long elapsedHours = different / hoursInMilli;
        different = different % hoursInMilli;

        long elapsedMinutes = different / minutesInMilli;
        different = different % minutesInMilli;

        long elapsedSeconds = different / secondsInMilli;

        return elapsedDays;

    }

    public static String AMPM(int i ){
        return (i == 0) ? "AM" : "PM";
    }

    public static String addZero(int i ){

        return ( i < 10) ? "0"+i : ""+i;
    }

    public static String convertToString(Date date){

        Calendar   cal = Calendar.getInstance();
        cal.setTime(date);
        return
                DateUtils.Month(
                        Integer.valueOf(
                                (String)android.text.format.DateFormat.format("MM", date)
                        ) - 1
                )
                        + " "
                        +  android.text.format.DateFormat.format("dd", date)

                        + ", "
                        +  cal.get(Calendar.HOUR)
                        + ":"
                        + DateUtils.addZero(cal.get(Calendar.MINUTE))
                        + " "
                        +  DateUtils.AMPM(cal.get(Calendar.AM_PM));



    }



}
