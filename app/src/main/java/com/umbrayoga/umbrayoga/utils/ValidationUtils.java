package com.umbrayoga.umbrayoga.utils;

import android.util.Patterns;
import android.widget.EditText;
import android.widget.TextView;

/**
 * Check the form fields before submitting
 */
public class ValidationUtils {

    public static boolean checkCredentials(EditText username, EditText password, EditText realName){
        boolean isSafe = true;

        String data = username.getText().toString();
        if(data.isEmpty()){
            isSafe = false;
            username.setError("Cannot be Empty");
        }

        if(!Patterns.EMAIL_ADDRESS.matcher(data).matches()){
            username.setError("Must be a valid Email");
            isSafe = false;
        }

        data = password.getText().toString();
        if(data.isEmpty()){
            password.setError("Cannot be Empty");
            isSafe = false;
        }
        if(data.length() <6){
            password.setError("Too short!");
            isSafe = false;
        }
        data = realName.getText().toString();
        if(data.isEmpty()){
            realName.setError("Cannot be Empty");
            isSafe = false;
        }

        return isSafe;
    }


    public  static boolean checkLoginCredentials(TextView username, TextView password){
        boolean isSafe = true;
        if (password.getText().toString().isEmpty()) {
            isSafe = false;
        }
        if (username.getText().toString().isEmpty()) {
            isSafe = false;
        }
        return isSafe;
    }

}
