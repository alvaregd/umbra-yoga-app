package com.umbrayoga.umbrayoga.activity;

import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.ProgressBar;

import com.umbrayoga.umbrayoga.R;
import com.umbrayoga.umbrayoga.adapter.BookFlowAdapter;
import com.umbrayoga.umbrayoga.callbacks.BookFlowCallbacks;
import com.umbrayoga.umbrayoga.data.ActivityData;
import com.umbrayoga.umbrayoga.data.BookingData;
import com.umbrayoga.umbrayoga.data.ScheduleData;
import com.umbrayoga.umbrayoga.data.StudioData;
import com.umbrayoga.umbrayoga.view.TabDotIndicator3;

public class BookFlowActivity extends BaseActivity implements BookFlowCallbacks {

    ViewPager mViewPager;
    TabDotIndicator3 indicator;
    ProgressBar progressBar;

    BookFlowAdapter mPagerAdapter;

    public BookingData getQueryParameters() {
        return queryParameters;
    }
    BookingData queryParameters;



    public ScheduleData[] getScheduleData() {
        return scheduleData;
    }

    public void setScheduleData(ScheduleData[] scheduleData) {
        this.scheduleData = scheduleData;
    }

    private ScheduleData[] scheduleData;


    /** Store queried data int memory*/
    private StudioData[] studioData;
    public StudioData[] getStudioData() {
        return studioData;
    }

    public void setStudioData(StudioData[] studioData) {
        this.studioData = studioData;
    }

    public ActivityData[] getActivityDatas() {
        return activityDatas;
    }

    public void setActivityDatas(ActivityData[] activityDatas) {
        this.activityDatas = activityDatas;
    }

    private ActivityData[] activityDatas;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_book_flow);

        progressBar= (ProgressBar)findViewById(R.id.pb_fetching);
        indicator = (TabDotIndicator3)findViewById(R.id.tabIndicator);
        mPagerAdapter = new BookFlowAdapter(getSupportFragmentManager(),this);

        mViewPager = (ViewPager) findViewById(R.id.pager);
        mViewPager.setAdapter(mPagerAdapter);
        mViewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                indicator.setguide(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });

        queryParameters = new BookingData();
    }



    /** Book Flow Selections */
    @Override
    public void onStudioSelected(String Studio, int selectedIndex) {
        queryParameters.studio = Studio;
        queryParameters.selectedStudioIndex =selectedIndex;
        mViewPager.setCurrentItem(1);
//        showProgressBar();
    }

    @Override
    public void onActivitySelected(String activity, int selectedIndex) {

        queryParameters.activity = activity;
        queryParameters.selectedStudioIndex= selectedIndex;
        mViewPager.setCurrentItem(2);
    }

    @Override
    public void onTimeSelected(String  time, int selectedIndex) {

    }

    public void hidePRogressBar(){
        progressBar.setVisibility(View.GONE);

    }

    public void showProgressBar(){
        progressBar.setVisibility(View.VISIBLE);
    }



}
