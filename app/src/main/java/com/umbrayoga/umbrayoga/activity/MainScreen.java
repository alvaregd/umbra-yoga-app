package com.umbrayoga.umbrayoga.activity;

import android.os.Bundle;

import com.umbrayoga.umbrayoga.R;

public class MainScreen extends BaseActivity {

    private final static String Tag = "MainScreen";

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_screen);
        currentFragment =getFragmentManager().findFragmentById(R.id.fragment);
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

}
