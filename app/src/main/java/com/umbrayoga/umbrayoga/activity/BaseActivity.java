package com.umbrayoga.umbrayoga.activity;

import android.app.Fragment;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;

import com.umbrayoga.umbrayoga.R;
import com.umbrayoga.umbrayoga.application.App;
import com.umbrayoga.umbrayoga.callbacks.AccountChangedListener;
import com.umbrayoga.umbrayoga.callbacks.DialogDismissListener;
import com.umbrayoga.umbrayoga.callbacks.LoginCallBack;
import com.umbrayoga.umbrayoga.fragment.LoginDialog;
import com.umbrayoga.umbrayoga.network.ParseLoginCallBack;

public class BaseActivity extends AppCompatActivity {

    private final static String Tag = "BaseActivity";

    protected Fragment currentFragment;
    protected LoginDialog dialogFragment;
    protected DialogDismissListener mDismissListener;
    protected AccountChangedListener mAccountListener;

    LoginCallBack mLoginCallBack = new LoginCallBack() {

        @Override
        public void onLoginSuccess() {
            if(dialogFragment != null)
                dialogFragment.dismiss();
            mAccountListener.OnAccountStateChanged(App.STATE_LOGGEDIN);
            invalidateOptionsMenu();
        }

        @Override
        public void onLoginFail(String error) {
            if(dialogFragment != null)
                dialogFragment.ReEnableUI(error);
        }

        @Override
        public void onLogout() {
            mAccountListener.OnAccountStateChanged(App.STATE_LOGGEDOUT);
            invalidateOptionsMenu();
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ((App)getApplication()).setmLoginCallBack(mLoginCallBack);



        mDismissListener = new DialogDismissListener() {
            @Override
            public void onDismiss() {
                dialogFragment = null;
            }
        };
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch(item.getItemId()){
            case R.id.action_login:

                ShowLoginDialog();

                break;
            case R.id.action_logout:
                ( (App)getApplication()).signOut();
                /** Create new Menu items */
                invalidateOptionsMenu();
                /** Recreate UI*/
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    public void ShowLoginDialog(){

        dialogFragment = LoginDialog.newInstance(0, (ParseLoginCallBack)getApplication(),mDismissListener);
        FragmentTransaction ft =  getFragmentManager().beginTransaction();
        dialogFragment.show(ft, "dialog");
    }

    public void setAccountListener(AccountChangedListener mListener){
        mAccountListener = mListener;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if(((App)getApplication()).isLoggedIn()){
            getMenuInflater().inflate(R.menu.menu_main_logout, menu);
        }else{
            getMenuInflater().inflate(R.menu.menu_main_login, menu);
        }
        return true;
    }

    public void injectListener(AccountChangedListener listener){
        mAccountListener = listener;
    }
}
