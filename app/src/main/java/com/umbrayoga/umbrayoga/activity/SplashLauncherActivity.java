package com.umbrayoga.umbrayoga.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.SystemClock;

import com.umbrayoga.umbrayoga.R;

public class SplashLauncherActivity extends Activity {

    private final static int DELAY = 1000;

    Class<? extends Activity> activityClass;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_splash);

        activityClass = MainScreen.class;
        newActivity = new Intent(getApplicationContext(), activityClass);
        newActivity.putExtra("logged", false);

        /** No, wait some time before going to the next activity */
        new Thread(new Runnable() {
            @Override
            public void run() {
                SystemClock.sleep(DELAY);
                startActivity(newActivity);
                finish();
            }
        }).start();
    }

    Intent newActivity;
}
