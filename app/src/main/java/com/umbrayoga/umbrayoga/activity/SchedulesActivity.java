package com.umbrayoga.umbrayoga.activity;


import android.app.Fragment;
import android.os.Bundle;

import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.Window;

import com.umbrayoga.umbrayoga.R;
import com.umbrayoga.umbrayoga.application.App;
import com.umbrayoga.umbrayoga.data.ScheduleData;
import com.umbrayoga.umbrayoga.fragment.ClassDetailFragment;
import com.umbrayoga.umbrayoga.callbacks.ClassDetailListener;
import com.umbrayoga.umbrayoga.callbacks.ListItemClickListener;
import com.umbrayoga.umbrayoga.fragment.ScheduleListFragment;
import com.umbrayoga.umbrayoga.network.Parse;
import com.umbrayoga.umbrayoga.network.ParseCancelbookingCallback;

public class SchedulesActivity extends AppCompatActivity implements
        ClassDetailListener, ListItemClickListener, ParseCancelbookingCallback {

    private final static String Tag = "ScheduleActivity";
    private final static boolean d_onCancelSuccess = true;
    boolean insideChildFragment =false;

    Fragment classDetailFragment;
    Fragment scheduleListFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_schedules);
        scheduleListFragment = getFragmentManager().findFragmentById(R.id.fragment);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
    }

    /** List Iitem Click Listener */
    @Override
    public void OnClassClicked(int id, ScheduleData attendanceDaata) {

        classDetailFragment = ClassDetailFragment.newInstance(0,this,attendanceDaata);
        getFragmentManager()
                .beginTransaction().replace(R.id.fragment,classDetailFragment )
                .addToBackStack(null)
                .commit();
        insideChildFragment = true;
    }

    @Override
    public void onBackPressed() {

        if(insideChildFragment){
            getFragmentManager().popBackStack();
            insideChildFragment = false;
        }else{
            super.onBackPressed();
        }
    }

    /** Class Detail User Listener */
    @Override
    public void OnClassCancelled(ScheduleData data) {

        Log.e(Tag,"OnClassCancelled() Enter");
        Parse.CancelBooking(data,((App)getApplication()).getUserId(),this);
        onBackPressed();
    }

    @Override
    public void OnAgreePressed() {
        onBackPressed();
    }

    /** Network success */
    @Override
    public void onCancelSuccess() {
       if(d_onCancelSuccess)Log.e(Tag,"onCancelSuccess()");

//        if(currentFragment instanceof ScheduleListFragment){
            if(d_onCancelSuccess)Log.e(Tag,"onCancelSuccess() IInstance of SCheduleListFragment");
            scheduleListFragment.onResume();
//        }

    }

    @Override
    public void onCancelFail(String message) {
        Log.e(Tag,"onCancelFail Msg:" + message);
    }
}
